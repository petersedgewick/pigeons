

 benny and joon


in bennys garage.

randy: so were planning our next vacation right. i want australia, she wants italy. i like snorkelling. she likes garlic. all of a sudden, out of nowhere, she says to me do i need her? jesus benny, what kind of question is that? i mean need, what does it mean to really need somebody?

eric: benny, fuel line.

benny: hey waldo, could you answer that phone?

waldo: yeah, hello?

postman: i need a check benny, c.o.d.

benny: in a minute, meet me in the office.

postman: (quietly) all right

benny: whoa, put that out, i got a fuel line broke.

waldo: hey benny, its joon on the phone again

benny: well tell her ill call her back, ask her what she wants

waldo: its an emergency, she says your running low on peanut butter super chunks

benny: (quietly) emergency

in the garage office

benny: (on phone) all right, the issue is jelly, you only like two kinds. you either like grape or poison berry, which? all right. yeah, yeah peanut butter, i know, super kind, all right, see you at 6. ok, bye.(hangs up phone) hi, thanks for waiting mam. erm, im afraid that the best deal we can do for you is going to be, erm, $7750. but i think if you check around youll see that that is a really good deal.

girl: yeah?

eric: that really is a very good deal mam. a little too good

girl: lets do it.outside benny and joons house, then inside.

(benny is walking home from work, he hears shouting inside)

joon: get out of my sight!

smeal: you wouldnt dare joon.

(joon screams and throws plate across room.)

benny: what is going on here? (he rushes in) joon! mrs smeal! mrs smeal, please.

smeal: beware the winds of reason shall blow stern.

benny: what?

smeal: i am done mr pearl, i am done. the mules turned to glue, she left the house unescorted, she has sudden outbursts. she is simply unmanageable

benny: mrs smeal! please, wait, please. let me talk to her. i can talk to her, you cant quit on such short notice.

smeal: oh, well im sorry sir. in ireland we have a saying when a boat runs ashore, the sea has spoken.

(mrs smeal leaves)in the kitchen, benny is cooking and on the phone to eric.benny: listen, if she is going to be alone every day until i find another housekeeper, im not leaving her alone at night too.

eric: listen bring her, whats she going to do? she paints and she reads.

benny: yeah, she paints, she reads, she lights things on fire.

eric: that happened once. look i got a fire extinguisher, just bring her. now get your ass over here, and dont forget the louie prang record. remember, always play for keeps.

benny: all right, all right, all right, ill be there.

benny: joon dinner! spaghetti and salad.

joon: in that order?

benny: what happened between you and mrs smeal?

joon: she was given the fits of semi-precious metaphors.

benny: the womans a house keeper joon, not an english professor.

joon: she moved things.

benny: she said you wandered off. did you leave the house alone?

joon: define alone

benny: you know full well what im talking about. listen, while im at work i cant be worried about whether or not your knocking another house keeper into early retirement. first it was mrs larkburg.

joon: a woman deeply and hygienically disturbed. her hair smelled.

benny: but then it was mrs piltz.

joon: piltz, the answers in the question. lets face it benjamin, hiring house keepers is not your forte.

benny: oh yeah, well until i do find another house keeper, you are going to stick close to the house, you understand? now have some salad. oh by the way, i want you to come with me to my poker game

joon: benny

benny: come on, youll have a good timethey travel to the poker game and joon sees sam in the tree. they arrive at the poker game

man: erm, wet belt buckle

mike: classical greek martini shaker

benny: 6 turkey feather darts

thomas: ill see you with the diana durban album

benny: come on

thomas: and ill raise you with the stethoscope

mike: does it have little plastic ear pieces on the end?

thomas: what do you care? you never win mike!

mike: well if i do, i dont want the, stabbing my ear drums. steering wheel knob

eric: salad shooter

benny: 56 debenture iron

thomas: foldo

mike: well you must have a good hand, huh

benny: no, im bluffing

mike: really

benny: come on, you got to play to find out, come on

mike: ok, 150ft of coaxial cable. show them

benny: full house, jacks over fours

eric: (laughing) oh shit

mike: i thought he was bluffing

thomas: well some one get some air to his brain. he was bluffing about bluffing

mike: youre a real pal benny, i tell you what, i got a solution to your house keeper problems, you can have my room mates.

joon: oh thats a terrific offer benjamin. to save you from the retched chore of smeal shopping. those places are such drudgery arent they? montgomery and smeal, smeals and roebuck, smeals r us

benny: joon. you got a roommate? when did you get a roommate?

mike: since last week. my cousin landed in town and my life has gone from heaven to hell in 7 days.

thomas: just like the good lord intended

mike: yeah, 20 years old and he cant even read. he spends all day polishing my plastic forks.

eric: what do you mean when you say he cant read? you mean words?

mike: yeah, and he keeps me up all night watching stupid old movies. my works starting to suffer.

joon: you installed cable tv.

benny: joon, would you please glue your sequins

joon: have we an internal sequin issue to deal with benjamin?

benny: can we make this the last hand? come on, come on, play cards.

eric: 2 tickets to tomorrow nights baseball game

benny: 1 pocket fisherman

thomas: fold

benny: do you ever stay in?

mike: a snorkel masknext morning in benny and joons house. joon is wearing a snorkel mask and makes a smoothie. she goes outside and holds up the traffic.

in the social workers officesw: she really knows how to rattle the day help

benny: you know, i think we need to find someone more understanding than mrs smeal

sw: now let me tell you something. how are you two getting along?

benny: us? fine, normal, you know fine. why?

sw: ill level with you. i think its time you gave serious consideration into a group home

benny: she already has a home

sw: benny, she needs to be with her peers

benny: yeah, she hates her peers. she has already quit the two day centres to prove it. look all we need right now is a new housekeeper

sw: well i know of no one else. youve been up and down the list. now you might want to entertain the possibilities that there are some people more capable of handling these episodes than you. i am beginning to that that you living together may not be the best thing any more

benny: im her brother and her only family. and weve done just fine the two of us for 12 years.

sw: yes but her stress level is always a factor in her display of symptoms. her agitation should be kept to a minimum

benny: everybody gets agitated sometimes its the only option

sw: benny, dont get me wrong im impressed that youve managed this long. but a group home would give her a chance to develop other relationships. also we dont know this but what if she was capable of a part time job? they would encourage her in that direction. these are very nice places, nurturing, supportive

benny: im not farming her outjoon has held up the traffic and the drivers are getting angry and shouting at her. a police man arrives

joon: i have every right to be outside officer. i have ever right.

police: yes mam. may i see some id please? in bennys garage

benny: hey waldo, did you start that brake job yet?

waldo: joon called. she says that youve run out of tapioca.

benny: she what?

waldo: oh, and the police will cooperate

benny: holy shit. listen jack, you want to go to the baseball game tonight,

jack: hey thanks benny.

benny: box seats. hello, joon?back at the house. joon is painting.

benny: time for your medicine joonbenny in his bedroom

benny: night steveat the garage

benny: oh hello, hows it going?

girl: i appreciate the deal on the engine

benny: oh yeah, no problem. is it running alright?

girl: yeah, its running great

benny: oh, good

girl: so can i erm, make it up to you with some dinner.

benny: some dinner? erm you serious?

girl: yeah, dinner with you

benny: erm, you know what i cant. i mean id like to but i cant

girl: ok

benny: ok

eric: stupid, stupidat the house playing table tennis

benny: 18:15

joon: 17:16

benny: 18:15

joon: it caught the corner.

benny: it was a foot off the table

joon: it was not

benny: juniper

joon: benjamin. dont underestimate the mentally ill. we know how to count

benny: ok, fine 17:16

joon: you cant give me what was already mine. youre trying to cheat

benny: im not cheating

joon: cheating

benny: joon, i dont cheat. youre the one whos cheating

joon: youre all trying to cheat me, youre trying to cheat. joon throws the racket and smashes a lamp. she then runs upstairs

at the hospitalthomas: (on the phone at the hospital) yeah, alright i spoke to some of the guys from the staff benny. nobody wants to be your house keeper. i will keep trying but i got to tell you i dont think so

benny: (on the phone at home) yeah, ok thanks. byejoon is playing with a fan and some paper. she lets go of the paper and it flies into a candle setting alight. benny puts it out and goes upstairs. he sits on the bed

benny: ok steviehe goes to feed his fish, but it is dead. he takes it to the bathroom and flushes it down the toilet.

outside erics housebenny: i mean i cant even keep a goldfish alive. what chance do i have with joon? if something bad happened to her id, id never ever be able to forgive myself

eric: a group home is the right thing for her benny.

benny: yeah, this is defiantly for her own good

eric: hey, its for both your own good. how much longer can you do this to your self? youre life is literally passing you by

benny: youre right. youre right. what am i doing with myself? i spend my life working on cars and taking care of my sister. i cant even have a relationship without having to worry how joon is going to fit in.

eric: you know, once you get settled in you can actually take a vacation. travel a little. i can hold down the fort no problem.

benny: travel? travel?

eric: benny, there is a whole world out there man, i mean picture yourself on the open road, wind in the hair in the alcamino. highway stretching out b- whats that? up ahead? a lovely motorist. stranded. carburettor trouble. better investigate.

benny: yeah right, im going to be stopping at a pay phone every 15 minutes to call joon to see how shes doing.at another poker game

mike: ok lets see, black sabbath over there

thomas: you got that flash light?

mike: its not a flashlight. it turns out its a brush kind of thing. hat

thomas: wheres the batteries?

mike: you didnt win any batteries. what are you talking about?

thomas: who gets the ice shaker? it goes on mine

mike: ok guys come one, hurry it up huh or we'll start without you

joon: ive been known to play a little cardsthey start to play cards

mike: box of 30 odd 6 cartridges

joon: medium sized green haired troll

thomas: soap on a rope, slightly used. cards?

mike: three

thomas: cards?

joon: two

thomas: the dealer takes three

mike: pass

joon: re-grout my shower, blue grout

thomas: i fold

mike: shampoo my dog, lava lamp

joon: scrape and paint the exterior of my house

thomas: hey hey come on slow it down you guys. these are supposed to be reasonable stakes

joon: thats a see and a raise, you follow me?

mike: you take my cousin off my hands

thomas: hey wait, wait, just stop, all right? bennys going to strangle you guys

joon: chill out thomas

benny: joon, we've gotta go

joon shows her cards

mike: a flush. (mike shows his cards) full house. yes, yes, oh benny, tonights your lucky night

thomas: mike warned it was for keeps benny

benny: what, whats for keeps?

joon: i lost

benny: what in the pot?

joon: a cousin

benny and joon are walking out of the house followed by mike

mike: hey guys, rules are rules, without them theres no order in the universe

benny: oh dont give me that crap. you took advantage

joon: of your sick sister. a heart flush is a perfectly respectable hand

mike: not respectable enough

benny: hey shut up mike. i am not taking this guy home

mike: you have to man. remember the bet i lost last year. i had to re-plant your socket set. i didnt back out did i?

benny: you cant bet a human being!

mike: oh, well if your going to be a baby about it

benny: all right mike. you want to play like that? ill take him home. i may not be responsible for what im going to do to him but ill take him.

mike: oh man, what are you doing you stupid jerk?

sam: oh theres something wrong with your car

mike: what did you do with my hob caps?

sam: i dont know. im sam

benny: so i here. im benny

sam: with an n?

benny: yeah two of em. this is joon

sam: with an n?

joon: one. youre out of youre tree

sam: its not my tree

benny: i think i need a beer

mike: hey benny, wait a minute. where are you going? come here man, give me a job. hey! youre going with them

at diner

sam dances with some rolls of bread

benny: oh god bless you.

sam knocks the tray out of the waitresss hands and catches it. he hands it to the customers but switches them over.

ruthie: hey pal, were trying to work here if you dont mind

sam: oh my god. ive just been looking for my boyfriend. have you seen him? hes a cute guy with a little mole on his right cheek.

man: ruthie one of your ghosts come back to haunt you?

sam: (screaming) oh brad, brad, brad please dont be dead. i never got a chance to tell you what you mean to me. oh brad please. its you youre you, ruthie melanie. co-star of the prom queen mutilator with dick beebe.

ruthie: you saw that?

sam: he was mine, he was mine. no sindy youre sick. sindy you need help, no sindy, no, no, no! (pretends to stab himself)

benny and joons house

sam looks around the room and sees joons paintings. he puts his finger in the wet paint and licks him finger.

joon: dont touch it

sam: its paint

joon: yes

sam: kirk douglas, van gough ear (makes a scissor action with his hand)

benny: these are yours. youll sleep here all right? come on its late. lets brush youre teeth and hair.

joon: i can brush my own teeth benjamin

benny: fine

joon: hair too

benny: perfect. come on

sam: oh, oh benny. hey benny. thanks for the couch. erm mike made me sleep under the sink.

benny: oh youre welcome. good night

sam: good night

next morning

sam is sleeping and joon is drawing him. he moves so joon quickly pits them away.

in the bathroom

sam: mentally ill. really?

benny: yeah. benny: but i mean dont worry about it. just let her go about her routine, you know. her routine is everyday therapy. oh and dont hang around her room while shes painting. she hates that.

sam: no

benny: she runs hot and cold on you, just ignore it. thats just the way it works. oh listen erm, she starts talking to herself, dont worry about it but dont answer.

sam: ok

benny: she sometimes hears voices in her head. that comes with the territory too. and erm just make sure that nothing and i mean nothing happens to her

sam: ok

benny: ok

benny goes out to work and leaves joon and sam. joon is making a breakfast smoothie and sam is watching as he walks in.

joon: having a boo radley moment are we?

she hands sam a glass of smoothie.

at garage

benny: good morning

eric: hey

benny: this today?

eric. yeah. so erm what did you do with the cousin? the bus station? the river? what? you throw him in the river?

benny: (laughs) no, i took him home

eric: you took him home?

benny: yeah

eric: to your house?

benny: yeah to my house

eric: what are you crazy?

benny: hey believe me its only temporary

eric: great, maybe he can tell joon about the group home

benny: listen, if im going to do this group home thing ive got to figure out the best way to tell her, you know what i mean. wheres waldo? is he not here yet?

eric: no

benny: all right thats it im going to have a talk with him

eric: no your not

benny: yes i am. im calling him right now. whats his phone number?

eric: 5550944

benny and joons house

sam is cleaning and joon is in the garden. she hears the loud music that sam is playing and runs inside.

joon: aah, too loud.

sam: aah, what? (joon points to the radio) oh.

he switches the radio off and joon takes it away.

benny walks home as he walks in he sees joon alone at the table

benny: hey wheres sam?

joon: i didnt mean to kick him out. i mean i didnt kick him out he just left.

benny: whoa, whoa, whoa what, what happened? did something happen?

joon: he just he just left. he was, he was in the air and, and, and with a thing and it was really loud. it was really loud. and all, i just kept seeing. he didnt mean to do it.

benny: do it? what? what, did he, what did he do?

joon: he cleaned the house

benny: and you kicked him out for that?

joon: (laughs)

benny: (laughs)

there is a knock on the door and a jack-in-the-box is left on the floor. joon picks it up. they both look up and see sam sat on the mail box.

joon: maybe i should invite him back in

benny: yeah before someone sticks a stamp on his head and mails him to guam.

back in the house

sam is making grilled cheese sandwiches with an iron

joon: some cultures are defined by their relationship to cheese

benny: is that a fact?

joon is in bed and benny walks in

joon: he can really cook cant he?

benny: uh-uh yeah. all for the grilled cheese i might of used the wool setting.

joon: thats what i told him

benny: really? what did he use?

joon: rayon. silk would have been too soggy. cotton would

benny: would of burned it

joon: right. fortunately he consulted me before giving it steam. i was four square against it. i wish he could be my new smeal.

joon hums a tune and they both start singing the words. it flashbacks to when they were kids and their parents died in a car crash.

benny: good night

joon: good night

the next morning and sam is writing a letter to him mum

joon: you need some help? dear mum, i have departed mikes with love and gratitude and am not living with benny and joon pearl, two magnificently devoted new friends. bennys pre-disposition to hyper tense irish monologue is sub standing and he has given me a job as a domestic engineer and is pleased with my performance so far. love sam

sam: wow, i didnt know how to talk like that

they go outside and post the letter

sam: tapioca?

joon: oh, yes

in the diner

ruthie: ok

sam: thank you ruthie

ruthie: youre welcome sam. let me know if you need anything ok?

sam: you dont like raisins?

joon: not really

sam: why?

joon: they used to be fat and juicy and now theyre twisted like they had their life stolen. they taste sweet but really they are just humiliated grapes. i cant say im a big supporter of the raisin council.

sam: did you see those raisins on tv? the ones that sing and dance and stuff

joon: they scare me

sam: yeah me too

joon; its sick, commercial people make them sing and dance so people will eat them

sam: its a shame about raisins

joon: cannibals

sam; yeah. do you like avocadoes?

joon: theyre a fruit you know.

sam: ruthie, do you got any avocadoes?

at benny and joons house

benny: joon

he looks outside and sees a car pull up. ruthie, sam and joon get out of the car.

joon: youre very pretty when youre off work ruthie

ruthie: thank you. maybe i should quit my job huh?

benny: where the hell have you two been?

ruthie: oh dont worry, they were with me. we were just running some errands.

benny: oh how you doing?

ruthie: hi

benny; running errands? you ran an errand?

joon: yes i did, i ran an errand.

joon hands benny two new fish

benny: oh look

joon; they are both named steven. one with a v and one with a ph.

sam: look, look, look ruthies movie

joon: yes and shes staying for dinner too

benny: come on in, come on in

by the river

benny: so why did you leave?

ruthie: la? i wasnt that good of an actress

benny: well thats not how sam tells it. hes raving about you

ruthie: yeah well, hes sweeter than he is judgemental. how long have you known him?

benny: sam? erm 72 hours

ruthie: be serious

benny: i am, serious

ruthie: really

benny: im always serious, im too serious. did you ever get to the point in your life when nothing makes any sense?

inside watching the movie

sam is talking along with the movie

outside trying to start ruthies car

benny: try it again. now theres only two reasons a car wont start. either youre not getting fuel or youre not getting fuel. youve got fire. i think youd better let me drive you home. sorry

ruthie: thanks

pulling up outside ruthies house

ruthie: here it is, right here

benny; 2nd job huh/

ruthie: yeah got to make those ends meet you know

benny: this is just how i imagined your place. well

ruthie: you want to come in? or you want to

benny: oh no ill come in, for a minute. whoa! what is this? nice porch.

ruthie: pretty nice fish joon gave you

benny: oh yeah, those fish. yeah i was shocked i was nice fish, i mean yeah

ruthie: i never had a fish

benny: what? not even as a kid?

ruthie: no, under privileged childhood i guess

benny: down right un-american if you ask me

ruthie: yeah

benny: you can have one of mine if you want

ruthie: no i couldnt possibly. they were a gift

benny: but you know you could borrow it. but you know we could share it. like joint custody. you could take it on the weekends you know. what is this?

ruthie: sit down son, youre making me nervous

benny: oh

ruthie: want a beer?

benny: yeah, you know what id better not. i gotta get goin. i better get goin ok?

ruthie: yeah ok

benny: ill take a rain check on the beer

ruthie: ok i got the doors

benny: want to go to dinner?

ruthie; ever been married?

benny: no

ruthie: yeah

benny: yeah? great. see you

ruthie: see you

benny: see you

benny and joons house

sam and joon kiss. sam blows up a balloon and lets the air out of it slowly so it makes a tune. it pops and they laugh

at the social workers office

sw: see you next week

joon: right thank you

sw: benny, would you come inside a minute?

benny: sure

sw: whos the new house keeper?

benny: the new oh! this friend just came to stay with us for a while but

sw: joon tells me hes a man

benny: well you know joon, how flowery she gets with words

sw: she shes lying? i mean is it a man or isnt it?

benny: well yes technically. hes really just a guy, a friend.

sw: so where does this leave the group home decision? i take it you havent talked to her about it yet

benny: no i erm

sw: the admission date is less than a week away

benny: but you see the thing is shes really doing fine. she seems content and everything. lets just leave it at that for now

sw: let me know when you want to talk to her about it

benny: ill let you know. ok bye

sam walks into the video store and takes the help wanted sign off the window

owner: hello

sam: i wanna help

owner: ah wonderful ok. if youd like to fill out this application ill be right with you.

sam picks up the pen and writes his name. he folds up the application and walks out of the shop.

benny and joon are back at the house. joon is scrubbing bennys finger nails.

benny: not so hard

joon: wow, you actually have cuticles

benny: i know its amazing. what the hell is he doing?

joon: mashed potatoes. so are you and ruthie considering item hood? are you? benjamin and ruthie sitting in a tree k-i-s-s-i-n-g. first comes love, then come marriage

benny: get out of here

joon: then comes little ruthamins and benjamins and babys 

benny: im just taking her out to dinner

sam is downstairs making mashed potatoes with a tennis racket

benny and ruthie are coming home from their date

ruthie: (laughing) thats a good joke

benny: you like that?

ruthie: uh-uh. what time you got?

benny: its eleven.

ruthie: on the nose?

benny: right exactly

ruthie: dont think ive ever asked anybody the time before and it be right on the nose

benny: (laughs) thats weird isnt it? its right on the nose

ruthie leans over and kissed benny

ruthie: do you, do you want to come in for a while? i erm, i got that beer waiting for you

benny: erm you know what? i should probably go

ruthie: ok

benny: listen erm can i tell you something?

ruthie: what?

benny: erm you see my life is just real complicated and

ruthie: give me a break benny i just offered you a bear ok

she gets out of the car and runs inside

benny: no

in the park the next day. sam, benny and joon are sat on a rug.

joon: why dont we pick up ruthie and get some ice cream?

sam: yeah lets get her

benny: no

sam: why?

benny: cos, she doesnt want to talk to me

sam: oh

he tries to put his hat on his head but it just pops off again. he does it to joon and tries to do it to benny.

benny: come on we should go. knock it off.

he knocks the hat out of sams hand and it goes flying. sam does an acrobatic show for the people in the park

benny: he is, hes incredible. hes amazing

joon: did you have to go to school for that?

sam: no no i got thrown out of school for that

joon: really?

sam: oh yeah

he stands on a bench and walks over it

benny: hey listen why dont you guys go on ahead? im going to hang around here for a while. ok?

joon: really?

benny: yeah go ahead. ill see you at home later. all right?

sam: ok

joon: ok

benny; that was, that was great.

sam: no no

benny: thank you

sam: no thank you. bye bye

benny tries to do the trick with the bench that sam did.

back at the house

sam and joon are sat on the sofa and kiss.

sam: joon

joon: what?

sam: i, i love you

joon: me too. dont tell benny

sam: ok

joon hears benny coming in so gets up and goes to her bedroom

next morning, benny is in the shower

benny: did you see the way you had everybody in the palm of youre hand? huh? youve got this gift and you shouldnt ignore it you should be out there doing something with it.

sam: like what?

benny: i dont know all i know is that they were saying this guy is something else. youre special. youre not some regular schmoe like the rest of us who do it because we have to do it. youre blessed

sam: benny,

benny: what?

sam: how sick is she?

benny: plenty sick. now listen to me, ive been doing some thinking

sam: because it seems to me that, well except from being a little mentally ill, shes pretty normal you know?

benny: do you realise you could be the next buster keeton?

sam is sat outside the video store contemplating whether to go in

sam goes in and talks to the owner. he has the job.

in the garage

benny: im telling you, randy listen to me, youve got to see this guy. tell him eric

eric: hes funny

benny: hes truly amazing. will you see him?

randy: yeah ill see him

benny: youll see him?

randy: yeah you never know, you know most of these guys work locally but some of them, the better ones, you can book in the out of town clubs

benny: you book them out of town?

randy: i book them all over. detroit, boston, sometimes la

benny: whoa! randy can you see him tonight?

randy: id love to see him tonight

benny: hi

ruthie: is my car ready?

benny: how you doing? oh come on in, come on in. erm listen erm i want to apologise for the other night. it came out all wrong

ruthie: no no its ok

benny: no i do

ruthie: hows my car?

benny: oh its running great. your fuel pump was shot so i threw in a new one. its running great

ruthie: how much do i owe you?

benny: ill tell you what. just gibe me $30 for the part and im not going to charge you for the labour. all right? ok?

ruthie: yes

benny: listen if youre not doin anything later i just lined up this thing for sam

ruthie: what thing?

benny: well its like an audition. maybe his chance to make a living at all this crazy stuff that he does. who knows? maybe he could travel, play clubs. i dont know, you know? so if youre not doing anything later and you feel like

ruthie: i dont know benny. i got food to deliver, apartments to rent. my lifes pretty complicated right now, you know? thanks. bye

at benny and joons house

benny: well lay out the whole routine right after dinner. you know what you could start with? remember the role thing you were doing at the diner?

joon: who died and made you ed mcmahon?

benny: whats your problem? this is his chance to do something, be somebody

joon: he is somebody

benny: yeah i know, but he wants to be more

joon: you dont know what he wants

benny: i know he doesnt want to be a house keeper for the rest of his life. if youre worried about getting a new smeal well get you a new smeal

joon: hes not my smeal

benny: house keeper, whatever

joon: we have to tell him

benny: what? tell me what?

sam: err bennyjoon and and i are you know.

benny: bullshit! you

he gets up and drags sam from the table

sam: no, no

joon: dont

benny: get the fuck out!

joon: you cant throw him out. i won him

benny: just settle down, just settle down

joon: i love him!

benny: yeah? well youre crazy!

joon: i am not crazy

benny; you see what weve come to? you see?

he gets out the group home leaflet

joon: i hate you, i hate you, i hate you, i hate you, i hate you, i hate you

in joons bedroom

benny: can i get you anything?

joon: tapioca

benny: tapioca? ok. anything on it?

joon: raisins

benny: ok

he goes downstairs and checks the fridge. there is none left so he goes out to get some. sam is sat in the tree outside and joon burns a picture of her and benny when they were children. sam goes to the door and joon goes out to him

joon: hi

benny arrives at ruthies house

benny: hi. how you doin?

ruthie: youre out late

benny: yeah. listen do you have any tapioca?

ruthie: jeez benny, ive heard a lot of lines in my time but

benny: no, no, no its not for me. its for joon

ruthie: come on in

benny: yeah?

ruthie: yeah

sam and joon get on the bus with two bags. they sit down and joon starts to talk to herself

joon: i cant hear you

sam: joon. ok?

joon: yeah

sam: good good good

he tries to calm her down

joon; im not no, no im not

sam: ok? all right? hold tight ok? shh. its ok. its ok

joon: i am not, i am not, i am not

joon starts to cry and sam is still trying to calm her down

joon: shut up!

sam: no, no, no, no. shh. shh. joon. please. come on

joon: no its not all in my head

sam: sir? sir! stop the bus! please stop the bus!

benny is driving back fromruthies and he sees the bus stopped. an ambulance arrives and the crew climb aboard the bus

joon: i dont want to talk to you

sam: joon

am1: all righty. you want to step out?

sam: yeah ok, ok

am1: how you doin hon.?

joon: you think i dont know who you are. i know just who you are. so just leave, leave. leave! dont touch me! dont touch me!

they grab her and put her in the ambulance. benny runs to the front of the crowd of people

benny: joon, joon!

he sees sam sat in the ambulance

at the mental hospital

benny: look i a going to see my sister. would you please tell me her room number?

woman: please sir, just come back in the morning

benny: let me talk to her god damn doctor

in the social workers room

sw: im sorry. she doesnt want any visitors. not you or anyone else

benny: she doesnt want to see me?

sw: no and she doesnt want to leave. benny why dont you go home? get some sleep. i promise ill keep in touch

he goes out of the room and sees sam in the waiting room

benny: i hope your happy. i hope youre happy with what you have done to her.

he grabs sam

benny: you just stay the hell away from my sister

sam: no, no

benny: want to know why everybody laughs at you sam? because youre an idiot. youre a first class moron

sam: youre scared

benny: im what?

sam youre scared. i can see it. and you know why? i used to look up to you but know i cant look at you at all.

it flashes to joon in bed and benny walking by the trains. it shows joon again but awake by the window and sam at ruthies. benny is playing with a jack-in-the-box back at the house and sam is outside the hospital.

at the video store

sam: i think that fred astatine is the way to go

lady: yeah?

sam: thats a good choice

lady: whos that woman who starred in the roman

sam looks up and sees benny

sam: audrey hepburn

lady: yeah sam. thanks a lot

sam: have a nice day. can i help you?

benny: yeah i hope so.

at the mental hospital

sam and benny are in a room. thomas walks in

benny: thomas, how is she? tell me the truth

thomas: shes been better benny

benny: well youve got to tell me what room number shes in

thomas: 335. but dont even think about it benny. its a closed ward. no visitors

sam: no weve got to find joon. please

benny: thomas youve got to get us in there, to see her

thomas: its impossible benny

benny: what do you mean its impossible? listen i just need 2 minutes with her

thomas: ill be fired

benny: thomas, im begging you as a friend

thomas: id love to help out but garveys going to be in there to check on her soon. my hands are tied. my hands are tied!

thomas leaves

benny: what room did he say?

sam: 335

at the waiting room they sit down. the door opens to the closed ward and sam throws his hat in the way of the door do it doesnt close. they go through and as they are walking through the corridors they hear some men coming the other way.

benny: shit. what are you doing?

sam slides down the hall towards the men

sam: mommy?

man1: what are you doing here?

man2: i dont think you are supposed to be here

man3: what room did you come out of?

the pick him up and carry him back through the door.

man1: get the door

man3: how did he get past security?

man1: hey, watch the door

the coast is clear so benny continues until he find joons room.

benny: joon, look, i know you want to be left alone but you do not belong in here. not how would you like to try living in youre own apartment?

joon: an apartment?

benny: yeah! theres an apartment available in ruthies building

joon: youd let me live in an apartment?

benny: thats up to you. im through making those decisions for you. listen i im sorry i was such a jerk. and about sam. i was wrong, dead wrong.

joon: i scared him away

benny: no you didnt. hes here

joon: he is not

benny: he is. he got me in here. hes somewhere in the building.

joon: he is not. youd never let me have him. god. why do you hate me so much?

benny: i dont hate you

joon: you just need me to be sick

the social worker enters the waiting room and goes over to the desk.

sw: juniper pearls chart please. thank you

thomas sees the men carrying sam away

thomas: hey wait! wally! wait! wait! wally put him down. hes mine. ive been looking for him

man1: you sure?

thomas: yeah ill take care of him. sam you cant be in here

sam: wheres joons room?

thomas: come on

sam: ive got to see her

thomas: come on, come on

the social worker is walking down the hall to joons room and sees benny

sw: can you give me any reason why i shouldnt have you arrested?

sam and thomas are outside

thomas points to joons room

thomas; thats the one

sam: yeah

thomas: see?

sam: ok

thomas: sam, where are you going?

in joons room

sw: benny, this is exactly what we were trying to avoid. by now joon would be in a caring and controlled environment. isnt it obvious that you two need some distance?

benny: what dont we ask joon what she wants?

outside

sam is climbing up the building

thomas: sam what are you doing? hey sam come on! get down right now!

in joons room

joon: i dont know exactly what i want. i do know that i am tired of everyone telling me what to do



outside

thomas: sam come down. dont touch that

sam winds up the window cleaners hoist

thomas: sam leave that. im begging you.

in joons room

sw: joon, we want whats best for you

joon: i know that

she sees sam swinging across the window

joon: i

outside

thomas: sam come down now. sam youre going to get in trouble

inside

joon: i think i want to try living in my own apartment

benny looks outside and sees sam. the social worker looks outside but doesnt see anything

sw: hmm, i dont know

outside

thomas: sam stop swinging

the line of the hoist starts to come loose

inside

sw: but im willing to let you try. ill prepare the papers for her release

benny: thanks doctor garvey

outside

thomas: come down sam!

the hoist breaks and sam falls down

thomas: whoa! whoa! are you ok?

sam: ouch, ouch

in the hospital

joon tries to say something but cant get the word out

benny: i know, me too. im going to go and check you out. ok?

joon: ok

joon goes out and sam is waiting for her in a wheel chair. he gets up and they hug

at joons apartment

benny pulls up outside and ruthie is sat on her porch. he hands her some white roses

ruthie: havent we tried this before

benny: yeah but my lifes a lot less complicated now. is joon inside?

ruthie: yeah

benny: ok

he looks in her room and sees sam and joon making grilled cheese with an iron. he leaves her some roses at her door.

the end




benny & joon
writers :   barry berman  lesley mcneil
genres :   comedy  romance
user comments








