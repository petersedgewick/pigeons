
movie: mewtwo strikes back ***this is not an official manuscript. 
this is a translation of an untranslated episode and is only 
released as a reference material to the pokémon series and games. 
all copyrights belong to nintendo, game freaks, and all creators 
of the movie***


	(opens when the camera is underwater.)

voice
who am i? i've been dreaming about this world which doesn't exist 
in my memory. 
	(mew swims by)
who are you? wait! am i apart of you...? or not...?
	(camera goes into a darker place with orange bubbles then 
it shows someone's eye open. human figures are seen outside where 
he is.)
where is this? who am i? who brought me here?
	
	(we now see who the voice belongs to, it's mewtwo.)

mewtwo
who am i and why am i here? i just appeared here. i haven't even 
been born to this world yet. who am i?
	(mewtwo opens his eyes completely and shatters the glass 
and all the wires fall of him!)

scientist
it has awakened!

mewtwo
did he do this? 

scientist
wonderful! mewtwo has been completed.

 

mewtwo
mewtwo?

scientist
that's you. we created you from a mew the rarest pokémon in the 
world! yes, that's the pokémon which is said to be the most rare 
of all pokémon. mew.

mewtwo
mew? is that my parent? my father? my mother?

scientist
you can say that, but not really. you've been made stronger based 
on a mew.

mewtwo
who is this? if neither my father nor mother. then was it god? 
did god create me?

scientist
in the world the only ones that can create new life is man and 
god.

mewtwo
who did this? humans made me?

scientist
this is truly a victory for science.

another scientist
with this we've proven our theory correct. we can continue the 
study.

third scientist
this place will become the new mecca.

scientist
	(all shaking hands)
i'm going to contact the other offices.

mewtwo
who am i? where is this place? what was i born for?
	(mewtwo floats up and starts destroying the whole lab. then 
tons of little metal hand like things come down to stop him but 
he just blows them up. soon the whole lab is in flames.)

scientist
to make the strongest pokémon ever.. that was our dream...
	(the whole lab blows up. giovanni, the leader of team 
rocket comes down in a chopper and confronts mewtwo.)

mewtwo
is this my power? i'm the strongest pokémon in the world. mew. am 
i stronger then you?

giovanni
you surly are the strongest pokémon in the world. but there is 
another that would be the strongest.

mewtwo
the humans?

giovanni

	(nods)
if you and a human were to cooperate then the world would be 
ours.

 

mewtwo
the world would be ours?

giovanni
if your power is set free the world would be ruined. you must 
control your power.

mewtwo
control?

giovanni
are you okay with destroying the world as it is with that power?

mewtwo
what should i do? 
	(giovanni smiles. mewtwo is at trhq getting loads of armor 
put on.)

mewtwo
control myself with the armor that protects me? what are you 
going to make me do?

giovanni
it's simple. all you have to do are the things everyone else has 
been doing...

mewtwo
and that is?

giovanni
fighting, destruction, and plunder. the stronger will win.

--------------------------------------------------------
mewtwo is sent into an arena to fight pokémon first an onix which 
mewtwo throws into a wall with his mind. then it shows mewtwo out 
in a field with a lot of wild tauros. he raises them up with his 
mind control and tr throws pokéballs out and catches them all. 
then it shows mewtwo in the gym again fighting alakazam, mewtwo 
does just what he did to onix. then came electron and he used an 
electric attack and mewtwo reflected it back at him. next came 
gary's arcanine and nidoking... he took care of them easily. next 
scene, back in giovanni's lab.
--------------------------------------------------------

mewtwo
what am i fighting for? what am i living for?

giovanni
you are a pokémon. pokémon made by humans, what else are you 
worth?

mewtwo
my worth? who am i? what do i live for?
	(mewtwo starts breaking free of the wires)

giovanni
what are you doing?

mewtwo
i was made by humans. but i, who was created by humans, am not 
even a pokémon.
	(mewtwo blows up the place and flies off while all his 
armor slowly falls of into the sea. he lands on a rock.)
who am i? where is this place? who asked to make me? who wished 
to make me? i hate everything that made me! so this is neither an 
attack not a declaration of war but revenge on you who made me!

----------------------------------------------------
introduction
----------------------------------------------------


	(brock is cooking lunch for everyone.)

narrator
ash and the others are on a journey training to be pokémon 
masters. it's a beautiful day so they take a break in a field.

ash
is it ready yet?

misty
ash you can help too.

ash
i can't i can't even move one millimeter because i'm so starved.

brock
stew well and raise slowly. you mustn't be in a rush with both 
soup or pokémon.
	(pikachu and togepy spot someone coming)

pirate
hey you! the boy over there! are you ash from pallet town?

ash
that's right.

pirate
can i have a pokémon battle with you?

ash

	(runs over to him)
okay! i must have a solid fight!

misty
i thought you couldn't move one millimeter?

ash
just one or two pokémon fights before breakfast.

brock
this is lunch, so that would be before lunch.

pikachu
pika..chu...


	(has the japanese pokémon intro music through this battle 
the whole time. ash pulls out a pokéball and throws out 
bulbasaur. then the pirate throws out donfan. bulbasaur charges 
and donfan rolls into a ball and hits bulbasaur, then turns 
around and hits him again! then he tries it again but bulbasaur 
uses solar beam and donfan faints. the pirate then sends out 
machamp. ash sends out squirtle. machamp kicks squirtle down and 
then goes to kick him again but squirtle dodges it and bubbles 
him till machamp faints. the pirate gets mad and throws 3 
pokéballs down. one is a venomoth another is a golem and the 
other is a pinsir. pikachu jumps off ashes shoulder and does 
thundershock and all 3 faint.)

 

pirate
oh my god! 
	(this was possibly the funniest part of the entire movie :)

ash
i did it!

misty
it was just because your opponent was weak.

brock
he didn't even raise them well


	(flash to team rocket who is on a cliff watching our 
heroes.)

james
pikachu gets stronger and stronger.

jessie
beautiful

meowth
we couldn't beat him even once. but the word surrender isn't in 
our dictionary.

jessie
i will get that pikachu.

james
but look at that. 
	(looks at them all eating)

meowth
i'm so hungry.

jessie
i got a frying pan...

meowth
what good is such a thing without meat and vegetables? it's just 
an iron board.

---------------------------------------------
a fearow with a camera flies over head then it shows inside some 
place where you can see ash and pikachu on the screens.
---------------------------------------------

 

lady
are you sending them an invitation?

mewtwo
certainly.
	(dragonite flies out of the control center with a purse. it 
flies over the sees and then over team rocket and they fall then 
it flies over ash and everyone's head knocking them all over then 
she lands.)

ash
what are you? 
	(dragonite hands ash an invitation)
this? for me?
	(he hits a button and a hologram of the lady is displayed)

lady
please forgive me for the abrupt letter.

brock
what a beautiful lady...

lady
since you are a promising pokémon trainer of the future, we'd 
like to invite you to a party of my master who is the strongest 
pokémon trainer in the world. the place is the new island, 
pokémon castle. please check the reply card for whether your 
coming or not. please accept the invitation for the strongest 
trainer in the world.

brock
beautiful lady...

misty
what do we do?

brock
lets go for it. we should go for it.

ash
i don't feel uncomfortable with being told i'm a promising future 
trainer.

misty
well then check yes on the card.


	(they check yes, and hand the invitation back to dragonite. 
dragonite takes the card and flies off but jessie, james and 
meowth take out a net in an attempt to steal this rare pokémon. 
ash's invitation falls out of dragonites purse.)

jessie
don't say goodbye without saying hello.

meowth
a post card?

---------------------------------------------
scene change. we see mewtwo waving his hand back and forth moving 
the clouds, causing a big storm to start. we then see mew 
sleeping in a bubble underwater as the bubble floats to the top 
and pops, mew wakes up and flies off. ash and everyone run out of 
the storm and into a ocean side house that has all the other 
trainers who were invited.
---------------------------------------------

a trainer
calm down kingler! cut it out!
	(our heroes run inside wet.)

ash
it was a beautiful day!

brock
the weather along this beach can change easily.
	(we notice a big crowd of trainers by the door and officer 
jenny standing with a woman talking to them all)

crowd
huh? the ferry has been canceled?

sweet
we can't go to the new island?!

crowd
oh no! we were invited to go!

officer jenny
be quiet everyone. here is the pier master.

boijer
i'm boijer. if you want to know about the sea ask the seagulls on 
the pier. no need to bother. just look at the flaw of the clouds. 
i've never seen such a hurricane in all my years.

officer jenny
is it that terrible?

boijer
i grew up on a port and i've never experienced such a storm. 
besides the storm is above new island offshore of this pier. i 
can't let you be placed in danger. that's my wish as the one who 
guards this pier.

officer jenny
as you heard the ferry has been cancelled.

umio
its okay with my pokémon. my pokémon are stronger in water. i can 
cross the sea.

boijer
wait. i'm the one who knows the sea and i say no!

officer jenny
she's right! besides if the pokémon were to get hurt now they 
wouldn't be able to get treatment at a pokémon center.

ash
what do you mean?

officer jenny
nurse joy at the pokémon center is missing. pokémon can't receive 
treatment without nurse joy. the poster over there is the missing 
person report
	(brock looks at it)

brock
beautiful... who? i've seen her somewhere before...
	(some of the trainers go outside in the storm wanting to 
cross the ocean. soroa gets onto his pidgeot and flies off. umio 
gets on his gyarados and swims across.)

officer jenny
stop! stop or i'll arrest you!
	(sweet gets on her dewgong and starts swimming away.)

boijer
there's no use trying to stop them. after all they're pokémon 
trainers. if they were children who would stop after being told 
to, they wouldn't have gathered here. let's just pray for there 
safety.
	(ash, brock, misty and pikachu run outside and see everyone 
leaving.)

ash
across the sea lives the strongest trainer in the world. i can't 
cop out now.

misty
but we can't cross this sea using our pokémon.

ash
right.
	(a small wooden ship pulls up with 2 people in it. guess 
who?)

jessie
want a ride? not saying that it's free but depending on the 
situation. i might allow you onboard. 
	(they get on and start going and meowth is dressed like a 
girl in front of the boat, then a huge wave and team rocket's 
costumes fall off. then the waves start getting really rough.)

ash
you guys! you guys again?!

brock
you guys appear at the very worst times!

ash
what do you want?

jessie
if we're asked what we want...

james
the answer to that is..

meowth
there's no time for saying that!
	(a huge wave flips the boat. misty shoots out staryu and 
swims to the surface.)

brock
ash! misty!
	(misty comes up and he grabs onto staryu.)

misty
where is ash and pikachu?!
(a big wave comes again, and they go back under and see as h 
swimming up with squirtle.)

 

ash
hold on tight everyone!
	(squirtle and staryu keep swimming under the waves until 
they see light. then it shows mew flying above the clouds. ash 
and everyone get to the castle and climb out of the water, and 
the mysterious lady is standing there.

lady
welcome to the new island. please show me your invitation.

ash
how's this?
	(he shows her).

b rock
that was you just as i thought!

lady
yes?

brock
that picture of nurse joy i saw in the missing person report at 
the pier was you.

misty
that reminds me, she looks like her.

lady
i don't know what your talking about. i've been serving this 
castle ever since i was born. come this way. the other guests are 
already waiting.

---------------------------------------------
they go inside and team rocket floats up from the water on top of 
weezing. then it shows mew playing with the windmill on top of 
the castle. meanwhile ash and everyone are inside the castle.
---------------------------------------------

lady
those are the trainers who have already arrived.

ash
only 3?

lady
the trainers who couldn't cross that storm aren't worth inviting.

brock
so then you tested us?

lady
please take out the pokémon from the pokéballs and be seated. you 
are the chosen trainers.

james

	(outside and the front door shuts)
well then, let's sneak into this castle...

jessie
the only way is to use the exit.

meowth
where's the exit?

jessie
over there...

meowth
the s..s..sewer!? i'm not a brown rat!

jessie
don't complain!
	(all of a sudden mew comes down from behind tr and looks at 
them. jessie turns her head but mew disappeared.)

james
what's wrong jessie?

jessie
nothing lets go.

---------------------------------------------
meanwhile...
---------------------------------------------

sorao
you came too?

ash
you?

sorao
i flew here. my pidgeot can cross such a hurricane in one flight. 
everyone greet them! 
	(all his pokémon say their names then they see a gyarados.)

umio
that's my gyarados. i rode him across the sea. those kind of 
waves are nothing for it. 

ash
gyarados is a brutal pokémon isn't it?

umio
yeah, but if you can handle it, no one is more reliable.

sweet
my pokémon are just the same over there. 
	(points to her blastoise, wigglytuff, and more. then all 
the lights turn off, there is a bright beam of light coming down 
the center of a huge spiral staircase.)

lady
that you for waiting everyone. the strongest pokémon trainer is 
coming. 
	(all of a sudden mewtwo starts floating down and all the 
pokémon get worried.)


lady
yes, this is the strongest pokémon trainer and strongest pokémon. 
mr. mewtwo.
	(the lights come back on and everyone can see him clearly.)

ash
mewtwo?

umio
a pokémon is a pokémon trainer? it can't be. 
	(it becomes apparent that mewtwo is telepathically 
controlling joy...)

mewtwo
i've decided my own rules.

misty
that voice!

brock
telepathy!
	(mewtwo raises umio up in the air and throws him into a 
pool 
	(using his mind)

umio
darn it! go gyarados!
	(gyarados attacks with hyper beam, it goes to mewtwo and 
mewtwo stops it and turns it around and it hits gyarados causing 
it to faint.)

mewtwo
that was easy. 
	(looks at joy)
i no longer have any need for you anymore.
	(joy faints into brock's arms)

brock
nurse joy.

nurse joy
where is this place? why am i here?

mewtwo
to make you take care of me, i took you from the pokémon center. 
a doctor who is familiar with pokémon is convenient. you were 
quite useful. you don't remember anything though.

brock
what did you say?

mewtwo
humans can be manipulated in any way with my power.

misty
with what power?

pikachu
pika!
	(meanwhile tr are in the castle and they find a strange 
room and they enter it.)


meowth
what is this place? 
	(they look and see a charizard, venusaur and a blastoise in 
big glass test tubes sleeping.)

meowth
cute!

james
is it really?

jessie
these aren't like any treasures i've seen.
	(jessie walks away and sits on some button and a computer 
turns on with a very fuzzy screen.)

computer
...our study... to continue in our research ... we would have to 
collect samples from our test subjects...
	(all of a sudden something grabs meowth and throws him in a 
machine)

meowth
help! help!

jessie
meowth!
	(both jessie and james start pull to get meowth out and 
they do get him out but not before the machine got 3 hairs from 
off of his tail.)

meowth
ouch! my hair!
	(on the computer screen a figure of a meowth shows up and 
the computer starts saying some weird stuff then in an empty test 
tube next to team rocket, a meowth falls down from a pipe into 
it)

all of tr
meowth?!

meowth
i am meowth.

james
then this is.... meowth's copy? 
	(mew appears behind team rocket again.)

computer
but the copy we created is more than we've imaged. the pokémon 
which we found.... in the depths... it's name is mew... we 
succeeded in finding fossilized eye lashes of a mew. from the 
gathered components we created mewtwo with this machine. the goal 
was to make the strongest pokémon with our own hands. that was 
our dream. but soon mewtwo displayed unmeasurable violence. its 
all over. the research office will be destroyed. the only way 
left to abandon this place and escape. 
	(the computer shuts off)

jessie
this is the research office?

james
i think so.

meowth
yeah

james
but there's nothing wrong with it.

jessie
or maybe something created it again.

james
but who?

mewtwo
i thought of working with humans once. but i was disappointed. 
humans are the worst creatures inferior to pokémon. if creatures 
that are weak and cruel like humans control this world, this 
planet will come to ruin. 

brock
are you saying like your gonna rule it?

mewtwo

	(shakes his head)
pokémon are no good either because this planet is ruled by humans 
our pokémon... pokémon who lived for humans.

pikachu
pika..pika pika!

mewtwo
what did you say? you're not being controlled? you're with the 
humans because you want to be? to be with them by itself is 
wrong.
	(mewtwo lifts pikachu up with his mind and throws him but 
ash jumps back and catches him!)

pikachu
pika!

ash
pikachu!

mewtwo
weak pokémon rely on humans.

misty
ash! are you ok?

ash
yeah. how dare you attack pikachu?!

soroa
we can get him no matter what kind of pokémon he is, as long as 
he's a pokémon, go ryhorn!
	(ryhorn charges at mewtwo and is 1 inch from him and mewtwo 
stops him and raises him into the air, and shoots ryhorn across 
the room!)

 

soroa
ryhorn!

mewtwo
it's no use. i was born as the strongest of any pokémon in the 
world.

ash
you don't know that before we try.

mewtwo
you wan to try?

ash
we can ask for nothing better.

--------------------------------------------------------
mewtwo eyes start glowing then it shows in the lab with team 
rocket in it charizard, venusaur, and blastoise come out of there 
test tubes and walk out the door. then mew flies out with them.
--------------------------------------------------------

james
was that a..?

jessie
mew?



mewtwo
any human who are aiming to be pokémon trainers go first. 
charizard, blastoise, venusaur.
	(the 3 copies raise up from the ground.)these are the 
evolved forms of the copies i've made. 

sweet
copies?

soroa
they are...?
	(everything starts shaking and a huge door opens and inside 
is a pokémon battle field.)

brock
a battle field? is he going to have a pokémon battle?

soroa
i've got venusaur for that copy one!

sweet
i've got blastoise for that copy one!

ash
i've got charizard too. charizard i chose you!
	(charizard comes out and gives mewtwo and evil look then 
shoots fire out at him but mewtwo makes a force field around him 
so it doesn't even touch him.)
charizard, that's a surprise attack. 
	(mewtwo shoots up water and the fire goes out.)

mewtwo
what a bad tempered charizard. so who's first?
	(soroa's venusaur walks out.)


soroa
i was careless before, but it won't go that way this time.


	(the copied venusaur walks out.)

soroa
venusaur! razorleaf!

mewtwo
vine whip.
	(copied venusaur's vines chop up the razor leafs then his 
vines wrap around venusaur and tosses him across the room!)

soroa
venusaur!

sweet
i'm next! go blastoise!
	(mewtwo points for copied blastoise to go.)

sweet
blastoise, hydro pump!
	(copied blastoise spins right threw the water and hits 
blastoise causes him to go crashing hard into the wall.)

sweet
blastoise!

 

misty
be careful ash! his skills are strong!

ash
i know.
	(mewtwo signals copied charizard to go.)

ash
charizard! lets fight with speed not power! okay go!
	(both charizard fly up. ashes charizard keeps using flame-
thrower but copied charizard dodges them all and keeps hitting 
charizard in the stomach.)

misty
where are the speed attacks?

brock
the enemy is to fast!
	(copied charizard grabs charizard and starts flying down to 
the ground!)

ash
charizard!

mewtwo
dive bomb slam!

charizard hits into the ground then gets up.. roars loudly.. then 
faints.

ash

	(runs out to charizard)
hold on charizard! are you okay?

mewtwo
both speed and power are lacking.
	(mewtwo then throws his hands up and 3 black pokéballs 
appear he shoots them all out and captures sweet's blastoise 
another gets soroa's venusaur and the other captures ashes 
charizard! a small gold statue lifts up and the 3 balls shoot 
down into the hole.)

ash
stop!

misty
are you taking the other's pokémon?!

mewtwo
taking them? no. i'm going to make stronger copies than the 
pokémon you are so proud of.
	(mewtwo raises his arms again and hundreds of black 
pokéballs appear!)
it's suitable enough for me.

brock
copies?

ash
stop! that's a violation of the rules!

mewtwo
don't tell me what to do. 
	(mewtwo looks at ash and throws him into brock)
i've decided my own rules.
	(mewtwo drops his arms and all the black pokéballs start 
flying everywhere)

brock
they're coming!

ash
run everyone!

--------------------------------------------------------
the pokéballs are everywhere catching the trainers pokémon, the 
gyarados was caught then the golduck, then seaking, then scyther, 
then hitmonlee, then sandslash the trainers are trying to protect 
there pokémon but there is nothing they can do! then dewgong was 
caught, then rapidash, then vileplume. then bulbasaur and 
squirtle are fighting them off, then wigglytuff was caught, then 
pidgeot and when they get caught the pokéballs fly back into the 
whole under the statue lifted up in the air.
--------------------------------------------------------

ash
i've got it! take your own pokémon back to your own pokéballs! 
return squirtle and bulbasaur! 
	(they both go back into their pokéballs)


 

mewtwo
it's no use.
	(2 black pokéballs go to ash, open, and capture the 
pokéballs bulbasaur and squirtle are in!)
nothing is impossible with the black pokéballs i have made. 
	(misty puts togepy in her bag and shuts it tight while 
brock holds vulpix close.)

nurse joy
this place is no good. let's run outside!
	(nurse joy, psyduck, misty and brock start running but then 
a black ball catches psyduck)

misty
my psyduck!
	(a black ball catches vulpix.)

brock
vulpix!
	(pikachu is being hunted down by tons of black pokéballs! 
then pikachu falls and is about to be caught! but ash jumps in 
front of him and the pokéballs hit him.)

ash
run pikachu! 
	(pikachu is running and starts climbing the spiral stairs 
with ash running after him. pikachu keeps using thundershock to 
blow up all the pokéballs around him but more keep coming! ash 
starts running up the stairs a little behind pikachu)
don't give up pikachu! 
	(pikachu gets tired and he accidentally falls of the 
stairs! ash jumps after him and is about to grab him but then a 
black pokéballs snatches pikachu! but ash catches the pokéball 
and then falls into some water but when he does the pokéballs 
slips out of his hands and goes into the hole. since it was the 
last pokémon the statue starts to drop but ash just barely makes 
it and jumps down into the hole after the pikachu in the ball! 
team rocket is still in the lab, which is where all the copies 
are going. they are amazed at all the copies dropping into all 
the test tubes everywhere but then ash falls down chasing after 
pikachu!)

jessie
brat?

ash
i'm sorry, i don't have time to deal with you today!
	(ash jumps and grabs pikachu's pokéball but right when he 
does about 25 little metal mini hand like things come down trying 
to keep pikachu, ash is fighting off the mini hands.)
let it go! darn it! let him go! let go of my pikachu! let go! 
	(all the hands break and the whole copy machine starts 
blowing up and pikachu comes out of his ball.)
pikachu!

pikachu
pika!

but then all the copy pokémon start coming out of there tubes 
because the whole copy machine is all blown up. all the copies 
start walking out of the room.

jessie
oh no! there coming out!

meowth
my copy to!

ash
those are all copies?

jessie
where are they all going?

all of a sudden there's an explosion and all the black pokéballs 
come out and all the original pokémon come out!

james
the real ones have come out to!

2 of the black balls open and 2 normal pokéballs come out, then 
those open and squirtle and bulbasaur come out! ash and pikachu 
runs over and starts laughing and hugging each other then ash 
stops and looks up and looks angry!

meanwhile up stairs...

mewtwo
humans, i won't take your lives too. leave here.

the doors open to the exit.

mewtwo
that is if you an make it out in that storm.

then there is an explosion and all the copied pokémon come 
walking out.

mewtwo
what happened here?

from the smoke, ash and all the original pokémon come walking 
out.

ash
i can not forgive...! i will not forgive you!

misty/brock
ash!

misty

	(sees psyduck)
psyduck!

brock

	(sees vulpix)
vulpix!

mewtwo
did you set them free?

ash
i will protect my pokémon, my friends.
	(ash charges at mewtwo and goes to punch him but mewtwo 
throws him down to the ground. ash gets up and tries again and 
mewtwo lifts him up.)

misty
ash!

brock
stop!
	(mewtwo throws ash into a huge statue but just as he's 
about to hit it -which we know would really hurt
a pink bubble stops him and ash bounces a few times on that.)

mewtwo
what?
	(mew flies up to ash and stares at him, ash then looks at 
mew and mew pops the bubble and ash falls -only like a foot then 
mew starts giggling.)
you're... 
	(mew makes another bubble and starts jumping up and own on 
it giggling and having a good time, then mewtwo makes a black 
electric ball and shoots it up and it pops mew bubble. mew turns 
around and looks at mewtwo confused. mewtwo makes another and 
blows up when it hits mew. mewtwo makes 3 more and throws them 
all up at mew, mew dodges them all and starts giggling again.)

brock
that's...

misty
pokémon?

mew flies down and looks at mewtwo

mewtwo
mew. the pokémon that's supposed to be the rarest pokémon in the 
world.

brock
mew? 
	(mew looks around at everyone... i love this guy!)

mewtwo
i was made from you. but i am the stronger one...i am the true 
one.
	(mew is still looking around acting like its paying no 
attention to mewtwo.)

brock
mew and mewtwo.

sweet
mewtwo was made from mew.

mewtwo

	(starts glowing)
i am the only one who will survive. 
	(mewtwo starts chasing mew everywhere shooting tons of 
those black electric balls at him. but mew keeps running away 
dodging everything.)

mewtwo
who don't you want to fight? the reason why you avoid fighting is 
because your afraid of me? 
	(mewtwo makes another black electric ball and shoots it at 
mew and knocks mew right in the face and mew goes flying in the 
distance. everyone is then shocked, but then the same type of 
ball only blue comes back and hits mewtwo and he goes into a 
wall. mew flies back down. and mewtwo floats up from the fire and 
rubble. tr enters the room.)

mewtwo
your attacks are ineffective against me. now is the time to 
decide which one of us is the true one, you the copy... which is 
the stronger? we were made stronger than the real ones.

mew
mew, mew..mew mew mew...mew, mew..mew mew mew

meowth
i see.

jessie
what is it saying?

meowth
the real one is real. if they fight only using bodies without 
skills. the true ones will not be beaten by their copies

mewtwo
the real one is real?.... fine! 
	(mewtwo shoots another black electric ball at mew but mew 
dodges it and just barely misses ash! ash then starts climbing 
down the statue.)
lets decide which is the real one without skills. you're the 
stronger ones, go! 
	(all the copies start attacking their originals. the copies 
are winning. mewtwo makes a visible blue force field around 
himself. mew does the same except its pink and they fly up high 
in the air and keep ramming into each other. then pikachu is 
looking around at the big copy war then he sees his copy standing 
there ready to fight him! pikachu can't believe it.)


nurse joy
what is this fighting? both the real ones and copies are alive 
now.

sweet
everyone is a living thing

brock
though they were created, there are living beings that live in 
this world.

misty
the real ones and the copies... but both of them are the same 
living things. there's no such thing as a win and losing.

james
if we were asked that, i wouldn't know what to say

jessie
i feel so sorry

james
they're bullying themselves.

jessie
its like seeing what we used to be.

james
its like seeing what we are now.

both
feels bad! 
	(meowth's copy walks up to meowth.)

meowth
its you! 
	(they take out there claws and are ready fight)
this is gonna hurt. but every pokémon is fighting each other, 
you're unfair.

copy meowth
meow meow...

meowth
what? i'm more unfair? it's because i'm not fighting with you? 
your nails would hurt more wouldn't they?

copy meowth
meow meow meow

meowth
you said tonight's moon would be round? you're right, it must be 
a full moon.

copy meowth
meow meow

meowth
talking about the moon at a time like this is tasteful. its 
philosophical.
	(meanwhile mew and mewtwo are still bouncing around 
fighting. and ash finally makes it back to the ground.)

ash

	(looks at everyone fighting and getting so tired)
that's enough! stop!

brock
ash!

ash
i've got to stop them.

brock
no. the battle will continue unless mew and mewtwo stop fighting.

nurse joy
animals don't hand over there territory to the same type of 
animal.

misty
oh no.

nurse joy
they will fight until they drive their opponents away. that is 
the way of living beings.

brock
that may be for living beings but mewtwo was made by humans.

misty
but now he's a living being. 

ash
now they are all living beings. mew and mewtwo, pikachu and that 
other pikachu. 
	(mew and mewtwo's fight finally goes on ground and they 
both take off there force field. mew and mewtwo both shoots a 
beam out at each other and starts to run between them not seeing 
the 2 beams coming!)
stop!

brock
ash! 
	(both beams hit ash and he falls to the ground and turns to 
stone.)

mewtwo
it can't be. a human tried to stop our fight. 
	(pikachu runs out to ash and starts shaking ash. then he 
thunder shocks him many times and ash still doesn't move. pikachu 
begins to cry. then all the other pokémon look at pikachu -
including the copies
and they start crying. all there tears float over and land on ash 
then last, pikachu's tears fall onto ash and ash regains his hold 
on life, and turns back to flesh from rock)

ash
pikachu!

pikachu
pika pi!!

mewtwo
actually, both you and i are pokémon that already exist.

mew
mew.

mewtwo
it might be better if no one knows about us, or any of this...

mew
mew...
	(mew and mewtwo start flying away and then all the copies 
pokémon lift up into the air and fly with them!)

ash
why are you going everyone?

mewtwo

	(flying away with all the copies)
we were born and alive. we will continue to live somewhere in 
this world. 
	(all of a sudden the whole castle fills up with bright 
light and everyone disappears......... now we are back where all 
the trainers what to go on the ferry.)

officer jenny
the ferry has been canceled! there's a hurricane coming!

nurse joy

	(walks through the crowd)
don't worry everyone. i'm going to open the pokémon center as a 
shelter. follow me those who need it.

brock
nurse joy, the officer, and ms. boijer. even more beautiful than 
usual when they are wet by the rain.

ash
but why are we here?

misty
i don't know. we're here because we're here.

ash
it doesn't matter. well... huh? 
	(everyone runs outside)
it can't be...

officer jenny
the hurricane... it's as if it never existed.

boijer
i can run the ferry for again tomorrow.

ash

	(looks up and sees a mew in the sky)
look.

misty
what? i can't see anything.

brock
besides the clouds.

ash
on the day i left to become a pokémon trainer, i saw a phantom 
pokémon... and now.. i've seen it again.

brock
phantom pokémon.

ash
yes, someday i will 
	(meanwhile, tr is still on the new island except now the 
castle is gone.)

jessie
well if its this empty...

james
clean and clear...

meowth
and different than it usually is. 

jessie/james/meowth
feels gooooooooooooooood!!!!!



------------------------------------------
end theme
------------------------------------------

where do i keep walking too? i stop, asked by the wind. efforts 
to celebrate your birthday increase, you left the one and only 
home town for a journey still seeking something even now. big 
eyes are shining there not with your but with someone else. many 
hellos and many good-byes. a bit of memory like an illusion. 
where do you keep walking to? asked by the wind as you look over 
the sky where am i going to keep walking? lets start walking with 
the wind! standing firmly on the ground lets go on forever... 
until catching the dream we're aiming for. standing firmly on the 
ground lets go on forever... until catching the dream we're 
aiming for. la la la la la la la la la la la la la la la...






pokemon: mewtwo returns
writers :   masamitsu hidaka
genres :   animation  adventure  family  fantasy
user comments








