


they




@font-face {
	font-family: courier;
<b>}
</b>@font-face {
	font-family: comic sans ms;
<b>}
</b>@page {mso-page-border-surround-header: no; mso-page-border-surround-footer: no; }
@page section1 {size: 8.5in 11.0in; margin: 1.0in 0in 1.0in 0in; mso-header-margin: .5in; mso-footer-margin: .5in; mso-paper-source: 0; }
p.msonormal {
	font-size: 12pt; margin: 0in 0in 0pt; font-family: "comic sans ms"; mso-style-parent: ""; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"
<b>}
</b>li.msonormal {
	font-size: 12pt; margin: 0in 0in 0pt; font-family: "comic sans ms"; mso-style-parent: ""; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"
<b>}
</b>div.msonormal {
	font-size: 12pt; margin: 0in 0in 0pt; font-family: "comic sans ms"; mso-style-parent: ""; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"
<b>}
</b><b>p.general {
</b>	font-size: 12pt; margin: 0in 1.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: general
<b>}
</b><b>li.general {
</b>	font-size: 12pt; margin: 0in 1.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: general
<b>}
</b><b>div.general {
</b>	font-size: 12pt; margin: 0in 1.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: general
<b>}
</b><b>p.sceneheading {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: "scene heading"; mso-style-next: action
<b>}
</b><b>li.sceneheading {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: "scene heading"; mso-style-next: action
<b>}
</b><b>div.sceneheading {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: "scene heading"; mso-style-next: action
<b>}
</b><b>p.action {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: action
<b>}
</b><b>li.action {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: action
<b>}
</b><b>div.action {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: action
<b>}
</b><b>p.charactername {
</b>	font-size: 12pt; margin: 12pt 2.25in 0pt 3.25in; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: "character name"; mso-style-next: dialog
<b>}
</b><b>li.charactername {
</b>	font-size: 12pt; margin: 12pt 2.25in 0pt 3.25in; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: "character name"; mso-style-next: dialog
<b>}
</b><b>div.charactername {
</b>	font-size: 12pt; margin: 12pt 2.25in 0pt 3.25in; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: "character name"; mso-style-next: dialog
<b>}
</b><b>p.parenthetical {
</b>	font-size: 12pt; margin: 0in 2.75in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: parenthetical; mso-style-next: dialog
<b>}
</b><b>li.parenthetical {
</b>	font-size: 12pt; margin: 0in 2.75in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: parenthetical; mso-style-next: dialog
<b>}
</b><b>div.parenthetical {
</b>	font-size: 12pt; margin: 0in 2.75in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: parenthetical; mso-style-next: dialog
<b>}
</b><b>p.dialog {
</b>	font-size: 12pt; margin: 0in 2.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: dialog; mso-style-next: action
<b>}
</b><b>li.dialog {
</b>	font-size: 12pt; margin: 0in 2.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: dialog; mso-style-next: action
<b>}
</b><b>div.dialog {
</b>	font-size: 12pt; margin: 0in 2.25in 0pt; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: dialog; mso-style-next: action
<b>}
</b><b>p.transition {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt 4.25in; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: transition; mso-style-next: "scene heading"
<b>}
</b><b>li.transition {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt 4.25in; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: transition; mso-style-next: "scene heading"
<b>}
</b><b>div.transition {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt 4.25in; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: transition; mso-style-next: "scene heading"
<b>}
</b><b>p.shot {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: shot; mso-style-next: action
<b>}
</b><b>li.shot {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: shot; mso-style-next: action
<b>}
</b><b>div.shot {
</b>	font-size: 12pt; margin: 12pt 1.25in 0pt; text-transform: uppercase; font-family: courier; mso-pagination: none; mso-layout-grid-align: none; mso-fareast-font-family: "times new roman"; mso-bidi-font-family: "times new roman"; mso-style-name: shot; mso-style-next: action
<b>}
</b>div.section1 {
	page: section1
<b>}
</b><b>
