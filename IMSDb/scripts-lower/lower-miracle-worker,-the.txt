



 the miracle worker


 written by

 william gibson
 



 july 8th, 1961

 

 

 1 â¢ night. ini . nursery.

 0
 we see three faces in lamplight, looking down. they have
 been through a long vigil and it shows in their tired eyes
 and disarranged clothing. one is a young gentlewoman with
 a sweet girlish face, kate keller; the second is an elderly
 doctor, stethoscope at neck, thermometer in fingers; the
 third is a gruff gentleman in his forties with chin whis-
 kers, captain arthur keller.

 doctor
 shet11 live.
 kate closes her eyes.
 i can tell you now, i thought she
 wouldn't,

 keller

 (indulgent)
 i've brought up two of them, but
 this is my wife's first, she
 isn't battle-scarred yet.

 kate
 doctor. will my girl be all right?

 0 doctor
 oh, by morning she'll be knocking
 down captain kellerfs fences again.

 kate
 is there nothing we should do?

 keller

 (jovial)
 put up stronger fencing, ha?

 doctor
 just let her get well, she knows
 how to do it better than we do.
 these things come and go in infants,
 never know why. call it acute
 congestion of the stomach and brain.
 kell ? moves after the doctor, we hear them off-camera;
 we see only kate's face hovering over us, her hand adjust-
 ing the blanket.

 keller
 i'll see you to your buggy, doctor.

 (continued)

 

 

 

 

 2

 1. continued

 doctor (off camera)
 0 main thing is the fever's gone.
 i've never seen a baby, more
 vitality, that's the truth --
 their voices and footsteps fade. kate is bent lovingly
 over the crib, which emits a bleat; her finger is play-
 ful with the baby's face.

 kate
 hush. don't you cry now, you've
 been trouble enough. call it
 acute congestion, indeed, i don't
 see what's so cute about a con-
 gestion, just because it's yours?
 we'll have your father run an
 editorial in his paper, the wonders
 of modern medicine, they don't know
 what they're curing even when they
 cure it. men, men and their battle
 scars, we women will have to --
 helen, helen captain, captain,
 will you come.
 (she screams)

 2. night. ext. side of keller house.

 keller standing lamp in hand watching the doctor's buggy
 recede in the night. suddenly from the house behind him
 comes a knifing scream. keller wheels, the scream comes
 again and the camera follows keller's run with the lamp
 across the yard.

 3. night. int. keller house.

 keller runs into the house and up the dark stairs.

 4. night. int. helen's nursery.

 keller bursts into the bedroom where kate is screaming
 at the crib, her look intent on the baby and terrible.

 keller
 katie, what's wrong?

 kate
 look.

 (continued)

 

 

 

 

 3
 c o.w- u ued
 she makes a pass with her hand in the crib, at the baby's
 eyes.

 i
 kate (cont'd)
 she can't see. look at her eyes.
 she takes the lamp from him, moves it before the child's
 face.
 she can't seel

 keller

 (hoarsely)
 helen.

 a k te
 or hear. when i screamed she
 didn't blink. not an eyelash --

 keller
 helen. heleni

 kate
 she can't hear youl

 yeller

 helenl
 his face has something like fury in it, crying the child's
 name; kate almost fainting presses her knuckles to her
 mouth, to stop her own cry.

 fade out

 5. day. int. kellel house - title shot.

 the stairs in the keller house. in the foreground we see
 the bannisters, in the background the wall. a shadow.of
 the erratic, staggering figure of 5 year old helen appears
 on the wall, moving slowly. the two negro children
 scamper past -- their excited laughter coming to us, in
 contrast to the mute helen. in cu, in the fg, helen's
 little hands come into the frame, grasping the bannister
 rail.

 6. day. ext. yard - title shot.

 high long shot lookin`, down on lines of bed sheets hung
 up in the keller yard to dry. it appears to be a kind of

 (continued)

 

 

 

 

 6. continued
 maze, the sheets blowing in the wind; and staggering among
 them is the figure of the child, helen 5 years old, reach-
 ing out, struggling, buffeted by the damp, blowing sheets,
 crane down towards the figure. helen lashes out, her
 face frightened, unknowing --- she pulls down a sheet, be-
 comes tangled in its folds, almost hysterical. in the bg
 kate comes rushing from the house -- to help her.

 7. night. int. living room keller house - title shot.

 there is a christmas tree with decorations and in a wide
 shot we see helen, 7 years old, reaching up into tree.
 at a window we see the faces of percy 8 years, and martha,
 7 years, looking in. helenrs hand gets a christmas ball
 and rips it down, shattering at her feet. crab in and
 around coming to the side of the tree, seeing helents
 hands, another ball, she pulls it down. we see her face,
 blurred out, through the pine branches and needles, and
 beyond her the other children watching.

 $. day. ext. afield - title shot.

 a wide high shot. we see the lonely expanse of field with
 0 a low grass sprouting up, in the bg is the keller house
 perhaps. going slowly across the field is the figure of
 the child helen, patty duke. from in under camera comes
 kate, following her.

 (continue script)

 9. omit.

 10. day. ext. backyard keller house and pump - title shot.

 scene will be out mos up until ringing of bell:
 now we are in leaf-dappled sunlight in the keller yard, on
 three kneeling children and an old dog around the pump.
 v iney with jug on way to barn to get milk.
 the dog is a setter named belle, and she is sleeping. two
 of the children are negroes, martha and percy. the third
 child is helen, quite unkempt, in body a vivacious little
 person with a fine head, attractive, but noticeably blind,
 one eye larger and protruding; her gestures are abrupt,
 insistent, lacking in human restraint, and her face never

 0

 (continued)

 

 

 

 

 5

 10. continued
 smiles. she is flanked by the other two, in a litter of
 0 paper-doll cutouts, and while they speak, helen's hands
 thrust at their faces in turn, feeling baffledly at the
 movements of their lips. the camera is on this; the
 dialogue is only background.

 martha

 (snipping)
 first itm gonna cut off this doe-
 torts leg, one, two, now then --

 percy
 why you cuttin' off that doctor's
 legs?

 martha
 i'm gonna give him a operation.
 now i'm gonna cut off his arms,
 one, two. now i'm gonna fix up
 she pushes helen's hand away from her mouth.
 you stop that.

 percy
 cut off his stomach, that's a
 good operation.

 0

 martha
 no, i'm gonna cut off his head
 first, he got a bad cold.

 percy
 ain't gonna be much of that doc-
 tor left to fix up, time you
 finish all them opera--
 but helen is poking her fingers inside his mouth, to feel
 his tongue; he bites at them, annoyed, and she jerks them
 away. helen now fingers her own lips, moving them in
 imitation, but soundlessly. again the camera on this.

 martha
 what you do, bite hand?

 percy
 that's how i do, she keep pokint.
 her fingers in my mouth, i just
 bite 'em off.

 (continued)

 

 

 

 

 6.

 10. conti

 :1artea
 -.that she tryin' do now?

 0

 peercy
 she tryint talk. she gonna get
 read. looka her try in' talk..
 illen is scowling, the lips under her fingertips
 moving in silence, growing more and more frantic,
 until in a bizarre rage she bites at her own fingers.
 this sends percy off into laughter, but alarms :martha.

 marti
 hey, you stop now.
 she pulls eelei' s hand down,
 you just sit quiet and
 but at once heleit topples ltartha on her back, knees
 pinning her shoulders down, and ;-ravs the sissors.
 martha screams, hile her tied b _nchlets of hair
 f17 off in snips of the scissors. percy darts to
 the bell stria on the porch, yanks it w the dell

 ripgs.
 boa. day. int. livid rooli

 e
 captain x ller is at work at his deal. ja es is at
 his ease in conversation with kate and aunt ev.
 kate is serving a cool refreshment. itar ringing
 of bell. kate rushes out the door followed more
 slowly by a"mitt w, leaving behind jails & feller.

 11. kate o? ?` ninc door onto arch sees the scone.
 five years have done much to her, the girlish play-
 fulness is gone, she is a woman settled in grief.

 bate
 (for the thousandth time)
 helen.
 she is dorm the steps at once to them, seizing
 ii le'tt s wrists and lifting her off iartha. vine=
 runs in and chases martha and percy off.

 0

 

 

 

 

 6a.

 ,

 11. 4 :'?1 Â± 17-t i)
 `kat_j (c o1'~t i d
 let me have those scissors,

 e
 sate reaches for the scissors in helen' s hand. put
 hel n pulls the scissors baci.', they stru g:le 'for
 then: a mno_m.ent, then kate gives up, lets hele1.1 keep
 them.
 she tries to drrrr helen into the house. helen
 jerks swwray. kate next goes do.-,rn on her knees, tskes
 ?ele! 1 s hands gently, and using; the scissors like
 a doll, m& es heli n caress and cradle them; she
 points =,y' s finger houseward.s. iiiï¿½len' s whole
 body now beco_nnes ea^er; she surrenders the scissors,
 kate t?'-rns her toward the door and -..hues her a little
 pus-1. i len scrambles up and toward the h_ï¿½u se, and
 na= risin- follows her,

 12. day. int. i,ell.er livinc rood.

 there is a cradle with a sleeping infant, t1irdred:
 c".pt_":in ke!,l:_r in spectacles is working over netrs-
 paper pages at a corner desk; a benign am1t ev,
 wearing a hat, with a sew in-,. basket on a sofa is put-
 tin the finishing stitches on a big shapeless doll
 made out of towels; an ineol ont young man of i"te' s
 are, ja s 1c_ tr turns from the window to look at
 h e l_,'n. i le halts, her hands alert to grope, and
 e kate turns her to the aunt, who gives her the doll,
 the aunt is meanwhile speaking to teller.

 aunt ev
 arthur, something ought to be done for
 that child.

 t-
 a refresh,; ng suggg;estion. 'Â°l .at?

 aunt ev
 '-thy, this ver-t famous perkins
 school in poston; they're just
 supposed to do wonders.

 ic'eller
 the child's been to speci lists
 everywhere. the- couldn't help her
 in baltimore or 1,1 -7aslaington, could
 they?

 

 

 

 

 7.

 12. co:' 'i]'' t j'

 0
 t'iin the cap;':ain will write to
 the perkins school soon.

 kell' r
 :ratie, howe many tines can you
 let- then brew.{ your heart?
 any number of tines,
 h t e foreroinz and follovrin" dialo,ue is peripheral to
 the we are on }el ' t 1 she sits on the or
 to explore the doll t71th h.r fingers, gravely, and her
 hand pauses over the face: this is no face, a blan
 area, and it troubles her. clos'_' up on her finger--
 tips searching for flaatures. she tans ouestioninsly
 f'o:' eyes, but no one notices. she then yanks at
 her _jfl''t1s dress, and taps amain viforousry for
 eyes.
 0 c o:t'! i icj n

 0

 

 

 

 

 12. continued

 0

 aunt ev
 what, child?
 obviously not hearing, helen commences to go around from
 person to person, tapping her eyes, but no one attends or
 understands.

 kate
 (no break)
 as long as there's the least
 chance. for her to see. or
 hear, or --

 keller
 there isntt. now i must finish
 here.

 kate
 i think, with your permission,
 captain, i'd like to write to
 the perkins school.

 keller
 i said no, katie.

 0 aunt ev
 why, writing does no harm, arthur,
 only a little bitty letter. to
 see if they can help her.

 feller
 they can't.

 kate
 we won't know that; to be a fact,
 captain, until afller you write.
 13. helen now is groping among thing.' on fell er t s desk, and

 paws his papers to the floor. kj mlim is exasperated.

 k i j,er
 katie.
 kate quickly turns helen away, and retrives the papers.
 i might as well tÂ°y to work in a
 henyard as in th.s house --

 (continued)

 

 

 

 

 9

 13. c ontinu.d

 james

 (placating)
 you really ought to put her away,
 father.

 kate
 (staring up)
 what?

 james
 some asylum. it's the kindest
 thing.

 aunt ev
 why, she's your sister, james,
 not a nobody --

 james
 half sister, and half-mentally
 defective, she can't even keep
 herself clean. it's not pleasant
 to see her about all the time.

 kate
 do you dare? complain of what
 0 you can see?

 keller
 (very annoyed)
 this discussion is at an end]
 helen gropes her way with the doll back to aunt ev.
 the house is at sixes and sevens
 from morning 'til night over the
 childl i want some peace here, i
 don't care how, but one way we
 won't have it is by rushing up and
 down the country every time someone
 hears of a new quack. i'm as
 sensible to this affliction as --

 14. helen with aunt ev
 fingering her dress, yanks two buttons from it.

 aunt ev
 helena my buttons.

 (continued)

 0

 

 

 

 

 10

 14. continued
 helen pushes the buttons into the doll's face. kate
 0 now sees, comes swiftly to kneel, lifts helen's hand to
 her own eyes in question.

 kate
 eyes?
 helen nods energetically.
 she wants the doll to have eyes.
 another kind of silence now, while kate takes pins and
 buttons from the sewing basket and attaches them to the
 doll as eyes. keller stands, caught, and watches
 morosely, aunt ev blinks and conceals her emotion by
 inspecting her dress.

 aunt ev
 my goodness me, i'm not decent.

 kate
 she doesn't know better, aunt ev.
 i'll sew them on again.

 aunt ev

 (indulgently)
 40 it's worth a couple of buttons,
 kate, look.
 helen now has the doll with eyes, and cannot contain
 herself for joy; she rocks the doll, pats it vigorously,
 kisses it.
 this child has more sense than
 all these men kellers, if there's
 ever any way to reach that mind
 of hers.

 15. helen suddenly scrambles toward the cradle, and un-

 hesitatingly overturns it.
 the swaddled baby tumbles out and captain keller barely
 manages to dive and catch it in time.

 keller
 helen!
 all are in commotion, the baby screams, but helen un-
 perturbed is lying her doll in its place, kate on her

 (continued)

 i 

 

 

 

 

 11

 15. continued
 knees, pulls her hands off the cradle, wringing them;
 0 helen is bewildered.

 kate
 helen, helen, you're not to do
 such things, how can i make you

 understand--

 keller
 (hoarsely, handing the
 baby to aunt ev)
 katie.

 kate
 how can i get it into your head,
 my darling, my poor --

 keller
 katie, some way of teaching her
 an iota of discipline has to be

 kate

 (flaring)
 how can you discipline an afflicted
 child? is it her fault?
 0 helen's fingers have fluttered to her mother's lips,
 vainly trying to comprehend their movements; we are
 close on these two.

 keller
 i didn't say it was her fault.

 kate
 then whose? i don't know what to
 dot how can i teach her, beat her
 -- until she's black and blue?

 keller
 it's not safe to let her run around
 loose. now there must be a way of
 confining her, somehow, so she can't---

 kate
 where, in a cage? she's a growing
 child, she has to use her limbs!

 keller
 answer me one thing, is it fair to
 mildred here?

 (continuim)

 

 

 

 

 12

 15. continued

 s

 kate

 (inexorably)
 are you willing to put her away?
 now helen's face darkens in the same rage as at herself
 earlier, and her hand strikes at kate's lips. kate
 catches her again, and helen begins to kick, struggle,
 twist.

 keller
 now what?

 kate
 she wants to talk, like -- be like
 you and me.
 she holds helen struggling until we hear from the child
 her first sound so far, an inarticulate weird noise in
 her throat such as an animal in a trap might make; and
 kate releases her. the second she is free, helen
 blunders away collides violently with a chair, falls,
 and sits weeping. kate comes to her, embraces, caresses,
 soothes her, and buries her own face in her hair, until
 she can control her voice.

 0 kate
 every day she slips further away.
 i don't know how to call her back.

 aunt ev
 oh, i've a mind to write to boston
 myself. if that school can't help
 her, maybe they'll know somebody
 who can.

 keller
 (presently, heavily)
 i'll write to perkins, katie.
 he stands with the baby in his clasp, staring at helen's
 head, hanging down on kate's arm.

 16. day. ext. boston station - shooting up into vestibule


 of train.
 m.r. anagnos on platform with annie's suitcase, is reach-
 ing up to help annie mount steps. camera is behind

 annie.

 0 (continued)

 

 

 

 

 13

 16. c 013tinum

 anagnos
 0 -- only that a suitable governess
 has been found here and will come.
 it will no doubt be difficult for
 you there, annie, but it has been
 difficult for you at our school too,
 hmmm?

 17. camera in vestibule - see annie and anagnos from rear.

 anagnos
 ...-this is my last time to counsel
 you, annie, and you do lack some --
 by some i mean all -- what, tact,
 or talent to benct. to others. and
 what has saved you on one or more
 occasions at perkins is that there
 was nowhere to expel you to.
 above annieis seat anagnos puts her suitcase, looking
 .down at her.
 your eyes hurt?

 0 18. cu annie - wearing smoked glasses.

 annie
 my ears, mr. anagnos.

 anagnos

 (severely)
 nowhere but back to that dreadful
 place where children learn to be
 saucy. annie, i know how unhappy
 it was there, but that battle is
 dead and done with. why not let it
 stay buried?

 annie

 (cheerily)
 i think god must owe me a resur-
 rection.

 anagnos
 (a bit shocked)
 what?

 

 

 

 

 14

 19. annie taps her brow.

 annie
 0 well, he keeps digging up that
 battle.

 anagnos
 that is not a proper thing to say.
 annie, be humble.
 he extends a gift to her..
 you'll need their affection, work-
 ing with this child.
 annie, not quite comprehending, looks at gift.
 a gift with our affection.
 annie opens a small box and sees a garnet ring. she
 looks up, blinking, then down.

 annie
 dear mr. anagnos,
 (her voice is trembling)

 i --
 but she swallows over getting the ring on her finger
 and cannot continue until she finds a woebegone joke.

 0
 well, what should i say -- i'm an
 ignorant, opinionated girl and
 everything i am i owe to you?

 anagnos

 (smiles)
 that is only half true, annie.

 annie
 which half?
 the train lurches. anagnos bends and kisses her on the
 cheek and says,

 anagnos
 goodbye. goodbye.

 annie
 (she calls after him

 loudly)
 i won't give them any trouble.
 i'll be so ladylike they won't
 notice i've come.

 (continued)

 

 

 

 

 15

 19. continued
 0 passengers behind annie turn reacting to her shout. she
 is conscious of this and sinks down'in seat. past annie,
 through window of train, we see anagnos join 5 or 6 blind
 girls on the platform, and the train departs.

 begin montage sequences of train shots.

 20. annie, traveling, sitting in seat of moving train.

 21. scene in filthy train, annie reading perkins report.

 seat different indicating another train.

 22. annie descending from train vestibule into darkness of

 station platform. in bg portion of sign "washington".

 23. annie attempting to sleep on a night train with perkins

 report over face.

 24. annie sitting on seat of moving train.
 suddenly train rushes into a tunnel, and darkness comes
 over the scene.

 young jimmie (vo)
 where are we going, annie?

 young annie (vo )
 jimmie.

 young jimmie (vo)
 where are we going?

 young annie (vo)
 i said i'm taking care of you.

 young jimmie (vo)
 where we go...

 dissolve to:

 25. stylized tewkesbury admissions office.
 young jimmie and young annie are standing in front of
 desk. we do not see man sitting behind desk, but we hear
 his voice.

 

 

 

 

 25. continued
 11al3' s voice
 annie sullivan, aged nine, virtually
 0 blind; jaynes sullivan, aged seven,
 -- what's the matter with your legs sonny?

 girl
 it's his hip, n ster, he was born
 that way.
 is an' s voice
 can't he walk without that crutch?
 the girl shakes her head.


 the end





miracle worker, the
writers :   william gibson
genres :   drama
user comments








