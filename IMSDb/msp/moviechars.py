import re
import operator


def movieChars(filename):

	f = open(filename, 'r')
	try:
		text = f.read()
	except UnicodeDecodeError:
		yield None
		return
	lines = text.splitlines()
	characters = dict()
	punctuation = [',', '.', '\'', '\"', '!', '?', '...', ':', ')', '(', '[', ']', '&']

	for line in lines:
		line = line.strip()
		if (line.isupper()):
			s1 = re.search('EXT\.', line)
			s2 = re.search('INT\.', line)
			if (not (s1 or s2)):
				if line in characters:
					characters[line] = characters[line] + 1
				else:
					characters[line] = 1
		
	temp = dict()
	for char, count in characters.items():
		if ((count > 4) and (not any([c in char for c in punctuation]))):
			temp[char] = len(re.findall(char, text, re.I))
	characters = temp.copy()
	del temp

	charlist = sorted(characters)
	for char in charlist:
		yield (char, characters[char])
			
