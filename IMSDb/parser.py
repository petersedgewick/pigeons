from msp.movieactors import actorGender
from msp.moviechars import movieChars
from msp.movietype import movieGenre
from pathlib import Path
import json

SCRIPT_DIR = "scripts"
OUT_DIR = "script-data.json"

with (Path(OUT_DIR)).open("w") as f:
	f.write('[\n')
	for file in Path(SCRIPT_DIR).iterdir():
		characters  = {x["character"] : {"gender": x["gender"]} for x in actorGender(file) if x is not None}

		for character, count in (x for x in movieChars(file) if x is not None):
			if character in characters:
				characters[character]['count'] = count

		print({"title": file.name.replace('.txt', ''),
			 "characters": characters,
			 "movie_genre": str(movieGenre(file))
			})
		f.write(json.dumps(
			{"title": file.name.replace('.txt', ''),
			 "characters": characters,
			 "movie_genre": str(movieGenre(file))
			}))
		f.write(',\n')
	f.write(']')
