#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from input import clean_string_split
from pathlib import Path
from string import punctuation, printable
import json
from enums import *


from gensim.corpora import Dictionary
from gensim.models import TfidfModel
# from gensim.similarities import MatrixSimilarity
# from gensim.matutils import corpus2dense, jensen_shannon

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

from gensim.corpora import Dictionary
from gensim.models import TfidfModel

MODEL_PATH = "."
OUT_PATH = "."
SCRIPT_DIR = "./IMSDb/scripts"

def clean_word(string):
    for p in punctuation:
        string = string.replace(p, '')
    return string.lower()

script_paths = Path(SCRIPT_DIR).iterdir()

# check which files are invalidly encoded - check them.
clean = {}
i = 1
for f in script_paths:
    i += 1
    print(i)
    script = clean_string_split(f.open("r", errors='backslashreplace').read())
    words = []
    for word in script:
        cleaned = clean_word(word)
        if cleaned is not '' and cleaned not in stop_words and len(cleaned) > 2 and not cleaned.isnumeric():
             words.append(cleaned)
    clean[str(f)] = words

with open('IMSDb/scripts-clean.json', 'w') as fp:
    json.dump(clean, fp)
    fp.close()