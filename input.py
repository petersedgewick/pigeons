import re
from enums import *

year_softness = 1


class Query:
    def __init__(self, years=None, actors=None, quotes=None, genres=None, tags=None):
        self.years = years if years is not None else []
        self.quotes = quotes if quotes is not None else []
        self.actors = actors if actors is not None else []
        self.genres = genres if genres is not None else []
        self.tags = tags if tags is not None else []

    def __str__(self):
        return f'Years: {self.years}, Quotes: {self.quotes}, Actors: {self.quotes}, Genres: {self.genres}, Tags: {self.tags}'


def parse(string: str):
    query = Query()
    string = string.lower()

    print("🔨 trying to parse query" + string)
    quotes = re.findall('\"(.+?)\"', string)
    if len(quotes) != 0:
        for quote in quotes:
            string = string.replace('"' + quote + '"', "")
            query.quotes.append(quote)
    for name in actors:
        if " "+name+" " in string or " "+name+"." in string or name+" " in string or name+"." in string:
            query.actors.append(name)
            print("🎯 got actors: " + str(query.actors))
            string = string.replace(name, '')
    for swap in genre_map:
        string = string.replace(swap, genre_map[swap])

    print("🧤 cleaning string")
    string = clean_string(string)

    for s in string.split(" "):
        if is_year(s):
            query.years += list(range(int(s) - year_softness, int(s) + year_softness + 1))
            print("🎯 got years: " + str(query.years))
        elif get_decade(s):
            query.years += get_decade(s)
            print("🎯 got years: " + str(query.years))
        elif is_genre(s):
            query.genres.append(s)
            print("🎯 got genres: " + str(query.genres))
        else:
            query.tags.append(s)
            print("🎯 added tag: " + str(query.tags))

    return query


def clean_string(string: str):
    cleaned = ''
    string = string.replace('.', '').replace("'", '').replace("’", '').replace(',','').replace('\n', '')
    string_words = string.split(" ")
    sorted_stop = list(reversed(sorted(stop_words, key=len)))
    for word in string_words:
        if word not in sorted_stop:
            cleaned += word + ' '

    return cleaned.rstrip()


def clean_string_split(string: str):
    '''
    Returns cleaned string as a kis
    '''
    cleaned = []
    string = string.replace('.', '').replace("'", '').replace("’", '').replace(',','').replace('\n', ' ').replace('\t', ' ')
    string_words = string.split(" ")
    sorted_stop = list(reversed(sorted(stop_words, key=len)))
    sorted_stop += ("\n", '\n\n', '\n\n\n\n')
    for word in string_words:
        if word not in sorted_stop:
            cleaned.append(word.lower())

    print('1')    
    return cleaned

def is_quote(string: str):
    if string in genres:
        return True
    return False


def is_genre(string: str):
    if string in genres:
        return True
    return False

def is_year(string: str):
    if re.match("([0-9]{2}|[0-9]{4})$", string):
        return True
    return False


def get_decade(string: str):
    matches = []
    finds = re.findall("([0-9]{2})s|([0-9]{4})s$", string)
    if not finds:
        return False

    match = [m for m in finds[0] if m is not '']
    if len(match) != 1:
        return False
    match = match[0]

    if len(match) is 2:
        for i in range(10):
            matches.append(int('19' + match[0] + str(i)))
    elif len(match) is 4:
        for i in range(10):
            matches.append(int(match[0:3] + str(i)))

    return matches
