#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from input import clean_string_split
from pathlib import Path
from string import punctuation, printable
from json import load

from gensim.corpora import Dictionary
from gensim.models import TfidfModel
# from gensim.similarities import MatrixSimilarity
# from gensim.matutils import corpus2dense, jensen_shannon

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

from gensim.corpora import Dictionary
from gensim.models import TfidfModel

MODEL_PATH = "."
IMG_DICT_PATH = "./ImgDict"
IMG_JSON_PATH = "./tagList.json"

img_data = load(Path(IMG_JSON_PATH).open())


docs = [c.get('tags', []) for c in img_data]
print("scripts retrieved")

dct = Dictionary(docs)
dct.save(IMG_DICT_PATH)

corpus = [dct.doc2bow(doc) for doc in docs]
print("training model")

model = TfidfModel(corpus)
model.save(str(Path(MODEL_PATH) / 'ImgModel'))
print("done")

