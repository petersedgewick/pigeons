from get import GetStill
import json

with open ("imdb2.json") as json_file:
    data = json.loads(json_file.read())

with open ("tagList.json") as json_f:
    old = json.loads(json_f.read())

tagList = [t for t in old if t != []]
old_movie_ids ={t['movieID'] for t in tagList if t != []}

for movie in data:
    
    movieID =  movie.get('movieID', "") 
    movieTitle =  movie.get('title', "")

    if(movieID == "" and movieTitle == ""):
        continue

    if(movieID in old_movie_ids):
        continue
    print(movieTitle)
    obj = GetStill()
    try:
        unflattened = obj.download(movieTitle, movieID)

        flattened = [item for sublist in unflattened for item in sublist]
        tags = {
            'movieID': movieID,
            'tags': flattened
        }
        tagList.append(tags)
    except TypeError as e:
        tagList.append({"movieID": movieID})
        print(e)
    except RuntimeError as e:
        print(e)
        tagList.append({"movieID": movieID})
    with open("tagList.json", 'w') as tagFile:
        json.dump(tagList, tagFile)




