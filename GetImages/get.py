from bs4 import BeautifulSoup
from imdb import IMDb
import requests
import shutil
import re
import json
import urllib.request
import os, sys
from PIL import Image, ImageOps
import torch
from io import BytesIO

from models import *
from utils import *

import time, datetime, random
import torch
from torch.utils.data import DataLoader
from torchvision import datasets, transforms
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image


class GetStill():
    def __init__(self):
        self.config_path = 'config/yolov3.cfg'
        self.weights_path = 'config/yolov3.weights'
        self.class_path = 'config/coco.names'
        self.img_size = 416
        self.conf_thres = 0.8
        self.nms_thres = 0.4
        self.model = Darknet(self.config_path, img_size=self.img_size)
        self.model.load_weights(self.weights_path)
        self.model.cuda()
        self.model.eval()
        self.classes = utils.load_classes(self.class_path)
        self.Tensor = torch.cuda.FloatTensor

    def resizeImage(self, image):

        width, height = image.size

        if width > height:
            left = width - height
            left = left / 2
            right = left
            top = 0
            bottom = 0
            image1 = image.crop((left, top, right, bottom))
            image2 = image.resize((256, 256), Image.ANTIALIAS)
            #image2.save(path,optimize=True,quality=85)

        else:
            left = 0
            right = left
            top = height - width
            top = top / 2
            bottom = top
            image1 = image.crop((left, top, right, bottom))
            image2 = image.resize((256, 256), Image.ANTIALIAS)
            #image2.save(path,optimize=True,quality=85)

        return image2

    def download(self, movie=None, movieID=None):

        ia = IMDb()

        BASE_URL = "https://www.imdb.com{}"
        URL = "https://www.imdb.com/title/tt{}/mediaindex?ref_=tt_pv_mi_sm/"

        if (movieID is None and movie):
            result = ia.search_movie(movie)
            firstResult = result[0]
            movieID = firstResult.movieID

        URL_MOVIE = URL.format(movieID)

        r = requests.get(URL_MOVIE)

        soup = BeautifulSoup(r.text, "html.parser")
        path = "../" + movie

        div = soup.find(class_="media_index_thumb_list")
        try:
            links = div.findAll('a', href=True)
        except AttributeError as e:
            print(e)
            return []


        allTags = []

        for i, link in enumerate(links):
            NEW_URL = BASE_URL.format(link['href'])
            img = link.find('img')
            source = img['src'].split('@')
            try:
                source = source[0] + '@.jpg'
                response = requests.get(source)
                image = Image.open(BytesIO(response.content))
                response.raise_for_status()
            except:
                source = img['src'].split('@')
                source = source[0] + '@@.jpg'
                response = requests.get(source)
                image = Image.open(BytesIO(response.content))

            filename = "../" + movie + "/" + movie + '-' + str(i) + '.jpg'

            try:
                img = self.resizeImage(image)
                tags = self.detect_image(img)
                allTags.append(tags)
            except:
                return allTags

        return allTags

    def detect_image(self, img):
        # scale and pad image
        ratio = min(self.img_size / img.size[0], self.img_size / img.size[1])
        imw = round(img.size[0] * ratio)
        imh = round(img.size[1] * ratio)
        img_transforms = transforms.Compose([
            transforms.Resize((imh, imw)),
            transforms.Pad((max(int(
                (imh - imw) / 2), 0), max(int(
                    (imw - imh) / 2), 0), max(int(
                        (imh - imw) / 2), 0), max(int((imw - imh) / 2), 0)),
                           (128, 128, 128)),
            transforms.ToTensor(),
        ])
        # convert image to Tensor
        image_tensor = img_transforms(img).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input_img = Variable(image_tensor.type(self.Tensor))
        # run inference on the self.model and get detections
        with torch.no_grad():
            detections = self.model(input_img)
            detections = utils.non_max_suppression(detections, 80,
                                                   self.conf_thres,
                                                   self.nms_thres)
        listTags = []
        if detections[0] is not None:
            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections[0]:
                tag = self.classes[int(cls_pred)]
                listTags.append(tag)

        return listTags
    
    


