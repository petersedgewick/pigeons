import random
import requests
from flask import Flask, request
from getContent import *
from pymessenger.bot import Bot
import os
import json
from input import *



app = Flask(__name__)
ACCESS_TOKEN = os.environ['PY_ACCESS_TOKEN']
VERIFY_TOKEN = os.environ['PY_VERIFY_TOKEN']
USER_MOVIE_MAP = {}
USER_QUERY_MAP = {}
USER_YR_MAP = {}
bot = Bot(ACCESS_TOKEN)


@app.route("/", methods=['GET', 'POST'])
def receive_message():
    print("\n")
    print("\n")
    print("\n")
    print(request.json)
    if request.method == 'GET':
        """Before allowing people to message your bot, Facebook has implemented a verify token
        that confirms all requests that your bot receives came from Facebook.""" 
        token_sent = request.args.get("hub.verify_token")
        return verify_fb_token(token_sent)
    #if the request was not get, it must be POST and we can just proceed with sending a message back to user
    else:
        # get whatever message a user sent the bot
       output = request.get_json()
       for event in output['entry']:
          messaging = event['messaging']
          for message in messaging:
            #Facebook Messenger ID for user so we know where to send response back to
            recipient_id = message['sender']['id']   
            user_name = get_user_info(recipient_id)
            print(user_name)
            if message.get('postback'): 
                if (message.get('postback').get('payload') == "GET_STARTED"): 
                    response_sent_text = f"Hi {user_name}, I am the Findr Bot that can find a movie title if you describe the movie to me."
                    send_message(recipient_id, response_sent_text)
                    response_sent_text = "Go ahead and describe me a movie in terms of: Actor, Year of release, Quotes or keywords or description of a scene"
                    send_message(recipient_id, response_sent_text)
                elif (message.get('postback').get('payload') == "START_AGAIN"):
                    response_sent_text = f"Hi {user_name}, I am the Findr Bot that can find a movie title if you describe the movie to me."
                    send_message(recipient_id, response_sent_text)
                    response_sent_text = "Go ahead and describe me a movie in terms of: Actor, Year of release, Quotes or keywords or description of a scene"
                    send_message(recipient_id, response_sent_text)
                elif (message.get('postback').get('payload') == "PREV"):
                    if(not recipient_id in USER_MOVIE_MAP):
                        response_sent_text = "I don't have history of your searches!"
                        send_message(recipient_id, response_sent_text)
                        response_sent_text = "Go ahead and describe me a movie in terms of: Actor, Year of release, Quotes or keywords or description of a scene"
                        send_message(recipient_id, response_sent_text)
                    else:
                        show_results(recipient_id, USER_MOVIE_MAP[recipient_id])
            if message.get('message'):
                if message['message'].get('text'):
                    new_message = message['message'].get('text')
                    response_sent_text = ""
                    # USER_QUERY_MAP.pop(recipient_id, None)
                    # USER_YR_MAP.pop(recipient_id, None)
                    if (not recipient_id in USER_QUERY_MAP):
                        print("user is new")
                        USER_QUERY_MAP[recipient_id] = new_message
                        USER_YR_MAP[recipient_id] = False
                        parsed_query = parse(new_message) 
                        response_sent_text = parse_query_obj(parsed_query)
                        if ( len(response_sent_text) != 0):
                            send_message(recipient_id, response_sent_text)
                        else:
                            USER_QUERY_MAP.pop(recipient_id, None)
                            USER_YR_MAP.pop(recipient_id, None)
                            final_respone = "Great! Here's the movie(s) that I think you are looking for:"
                            send_message(recipient_id, final_respone)
                            # Call the classifier
                            movies = get_movies(parsed_query)
                            if( not recipient_id in USER_MOVIE_MAP):
                                USER_MOVIE_MAP[recipient_id] = movies
                            else:
                                USER_MOVIE_MAP[recipient_id] = USER_MOVIE_MAP[recipient_id] + movies
                            show_results(recipient_id, movies)
                            final_respone = "You can start looking for another one, describe it to me"
                            send_message(recipient_id, final_respone)
                    else:
                        print("user is coming back")
                        parsed_query = parse(USER_QUERY_MAP[recipient_id])
                        if( new_message != "Nope"):
                            parsed_query = parse( USER_QUERY_MAP[recipient_id] + " " + new_message)
                        if ( len(parsed_query.years) == 0 and not USER_YR_MAP[recipient_id] ):
                            print("Sending buttons")
                            USER_QUERY_MAP[recipient_id] = USER_QUERY_MAP[recipient_id] + " " + new_message
                            send_year_buttons(recipient_id)
                        else:
                            response_sent_text = str(parsed_query)                        
                            USER_QUERY_MAP.pop(recipient_id, None)
                            USER_YR_MAP.pop(recipient_id, None)
                            final_respone = "Great! Here's the movie(s) that I think you are looking for:"
                            send_message(recipient_id, final_respone)
                            # Call the classifier
                            movies = get_movies(parsed_query)
                            if( not recipient_id in USER_MOVIE_MAP):
                                USER_MOVIE_MAP[recipient_id] = movies
                            else:
                                USER_MOVIE_MAP[recipient_id] = USER_MOVIE_MAP[recipient_id] + movies
                            show_results(recipient_id, movies)
                            final_respone = "You can start looking for another one, describe it to me"
                            send_message(recipient_id, final_respone)
                #if user sends us a GIF, photo,video, or any other non-text item
                if message['message'].get('attachments'):
                    response_sent_nontext = get_message()
                    send_message(recipient_id, response_sent_nontext)     
    return "Message Processed"

def send_year_buttons(recipient_id):    
    qk_reply = []
    USER_YR_MAP[recipient_id] = True
    text= "Do you recall the year range the movie was released in?"
    for i in range(5, 10):
        qk_reply.append({"content_type":"text", "title": f"{i}0s", "payload": f"{i}0s"})        

    qk_reply.append({"content_type":"text", "title": "2000s", "payload": "2000s"})
    qk_reply.append({"content_type":"text", "title": "2010s", "payload": "2010s"})
    qk_reply.append({"content_type":"text", "title": "Nope", "payload": "None"})

    user_url = f"https://graph.facebook.com/v2.6/me/messages?access_token={ACCESS_TOKEN}"
    data = {'recipient':{'id': f"{recipient_id}"}, "messaging_type": "RESPONSE", "message":{ "text": text, "quick_replies":qk_reply} } 
    # data = {'recipient':{'id': f"{recipient_id}"}, "message":{"attachment":{ "type":"template", "payload":{"template_type":"generic", "elements":[{"title":text,"buttons": buttons}] }}}}
    # data = {'recipient':{'id': f"{recipient_id}"}, "message":{"attachment":{ "type":"template", "payload":{"template_type":"generic", "elements": elements }}}}
    print("Buttons being sent")
    print(json.dumps(data))
    requestpost = requests.post(url = user_url, json=data)
    print("Buttons sent")
    print(requestpost)
    print(requestpost.json())
    #bot.send_button_message(recipient_id, "Do you recall the year range the movie was released", buttons)


def set_persistent_menu():
    strt = {"persistent_menu": [{"locale": "default","composer_input_disabled": False, "call_to_actions": [{"type": "postback", "title": "Start again","payload": "START_AGAIN"},{"type": "postback", "title": "Previous Results","payload": "PREV"} ]}]}    
    user_url = f"https://graph.facebook.com/v2.6/me/messenger_profile?access_token={ACCESS_TOKEN}"
    requestpost = requests.post(url = user_url, json=strt)
    print(requestpost)
    print(requestpost.json())

def get_movies(parsed_query):
    movies = []
    mv = {}
    obj = Content()
    result = [{"name": "Matrix", "id": "0133093" }]
    for movie in result:
        mv["title"] = movie["name"]
        mv["trailer"] = obj.getTrailer(movie["id"])
        mv["image_url"] = obj.getImage(movie["id"])
        stream = obj.getStream(movie["name"])        
        mv["netflix"] = stream["netflix"]
        mv["prime"] = stream["prime"]


    movies.append(mv)
    return movies
    # mv["title"] = "Terminator: Dark Fate (2019)"
    # mv["trailer"] = "https://www.imdb.com/videoplayer/vi3787112217?playlistId=tt6450804&ref_=tt_ov_vi"
    # mv["image_url"] = "https://m.media-amazon.com/images/M/MV5BNzhlYjE5MjMtZDJmYy00MGZmLTgwN2MtZGM0NTk2ZTczNmU5XkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_SX640_CR0,0,640,999_AL_.jpg"
    # mv["netflix"] = "https://www.netflix.com/gb/title/1032625"
    # mv["prime"] = "https://www.netflix.com/gb/title/1032625"



# obj.getStream("Matrix")
# obj.getTrailer("0133093")


def show_results(recipient_id, movies):
    elements = []    
    
    for movie in movies:
        element = {}
        buttons = []
        buttons.append({"type":"web_url", "title": "Watch Trailer", "url": movie["trailer"]})
        if( not movie["netflix"] is None):
            buttons.append({"type":"web_url", "title": "Watch on Netflix", "url": movie["netflix"]})
        if( not movie["prime"] is None):
            buttons.append({"type":"web_url", "title": "Watch on Amazon", "url": movie["prime"]})
        element["title"] = movie["title"]
        element["image_url"] = movie["image_url"]

        element["buttons"] = buttons
        elements.append(element)

    user_url = f"https://graph.facebook.com/v2.6/me/messages?access_token={ACCESS_TOKEN}"
    # data = {'recipient':{'id': f"{recipient_id}"}, "message":{"attachment":{ "type":"template", "payload":{"template_type":"generic", "elements":[{"title":text,"buttons": buttons}] }}}}
    data = {'recipient':{'id': f"{recipient_id}"}, "message":{"attachment":{ "type":"template", "payload":{"template_type":"generic", "elements": elements }}}}
    print("Buttons being sent")
    print(json.dumps(data))
    requestpost = requests.post(url = user_url, json=data)
    print("Buttons sent")
    print(requestpost)
    print(requestpost.json())
    #bot.send_button_message(recipient_id, "Do you recall the year range the movie was released", buttons)



def parse_query_obj(parsed_query):
    response_sent_arr = []
    response_sent_text = ""
    count = 0
    if ( len(parsed_query.years) == 0 ):
        response_sent_arr.append('Year')
        count += 1
    if ( len(parsed_query.actors) == 0 ):
        response_sent_arr.append('Actors')
        count += 1
    if ( len(parsed_query.genres) == 0 ):
        response_sent_arr.append('Genres')
        count += 1
    if ( len(parsed_query.tags) == 0 ):
        response_sent_arr.append('Excerpts or Keywords')
        count += 1
    print(count)
    print(response_sent_arr)
    if (count >1 ):
        response_sent_text = 'Do you recall anymore from the movie? Especially: ' + ", ".join(response_sent_arr)
    print(response_sent_text)
    return response_sent_text

def verify_fb_token(token_sent):
    #take token sent by facebook and verify it matches the verify token you sent
    #if they match, allow the request, else return an error 
    if token_sent == VERIFY_TOKEN:
        return request.args.get("hub.challenge")
    return 'Invalid verification token'

def set_get_started():
    strt = {"get_started":{"payload":"GET_STARTED"}}
    user_url = f"https://graph.facebook.com/v2.6/me/messenger_profile?access_token={ACCESS_TOKEN}"
    requestpost = requests.post(url = user_url, json=strt)
    print(requestpost)
    print(requestpost.json())

def get_user_info(recipient_id):
    user_url = f"https://graph.facebook.com/{recipient_id}?fields=first_name,last_name&access_token={ACCESS_TOKEN}"
    requestpost = requests.get(url = user_url)
    user_resp = requestpost.json()
    if user_resp.get("first_name"):
        return user_resp["first_name"]
    return "there"
    

#chooses a random message to send to the user
def get_message():
    sample_responses = ["You are stunning!", "We're proud of you.", "Keep on being you!", "We're greatful to know you :)"]
    # return selected item to the user
    return random.choice(sample_responses)

#uses PyMessenger to send response to user
def send_message(recipient_id, response):
    #sends user the text message provided via input response parameter
    bot.send_text_message(recipient_id, response)
    return "success"

if __name__ == "__main__":
    app.run()
