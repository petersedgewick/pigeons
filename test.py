import unittest
from input import *


class TestInput(unittest.TestCase):
    def tearDown(self):
        query = None

    def test_agenres(self):
        string = "romcom action"
        expected = Query(genres=["romance", "action"])
        actual = parse(string)
        self.assertTrue(actual.genres == expected.genres)

    def test_year(self):
        string = "1995"
        expected = Query(years=[1994, 1995, 1996])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)

    def test_decade_short(self):
        string = "90s"
        expected = Query(years=[1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)

    def test_decade_long(self):
        string = "2000s"
        expected = Query(years=[2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)

    def test_multiple_decades(self):
        string = "90s 2000s"
        expected = Query(years=[1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)

    def test_year_actor(self):
        string = "1995 Angelina Jolie"
        expected = Query(years=[1994, 1995, 1996],
                         actors=["angelina jolie"])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)

    def test_batman(self):
        string = "Heath Ledger 2008"
        expected = Query(years=[2007, 2008, 2009],
                         actors=["heath ledger"])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.actors == expected.actors)

    def test_decade_genres(self):
        string = "90s action"
        expected = Query(years=[1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999],
                         genres=["action"])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.genres == expected.genres)

    def test_quotes(self):
        string = '"Keep the change ya\' filthy animal" "Hence the expression greedy as a pig"'
        expected = Query(quotes=["keep the change ya\' filthy animal", "hence the expression greedy as a pig"])
        actual = parse(string)
        self.assertTrue(actual.quotes == expected.quotes)

    def test_all_together(self):
        string = '60s "Is It Just Me Or Is It Getting Crazier Out There?" thriller action joker'
        expected = Query(quotes=["is it just me or is it getting crazier out there?"],
                         years=[1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969],
                         genres=["thriller", "action"],
                         tags=["joker"])
        actual = parse(string)
        self.assertTrue(actual.quotes == expected.quotes)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.genres == expected.genres)
        self.assertTrue(actual.tags == expected.tags)

    def test_stop_words(self):
        string = 'that 1995 film where the actor flies with an umbrella'
        expected = Query(quotes=[],
                         years=[1994, 1995, 1996],
                         genres=[],
                         tags=["flies", "umbrella"])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.tags == expected.tags)

    def test_ford_v_ferrari(self):
        string = "One of the key actor's name is Christian Bale. It's an action movie and there are race car in the movie. It is released on 2019."
        expected = Query(quotes=[],
                         years=[2018, 2019, 2020],
                         genres=["action"],
                         actors=['christian bale'],
                         tags=['race', 'car'])
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.genres == expected.genres)
        self.assertTrue(actual.tags == expected.tags)
        self.assertTrue(actual.actors == expected.actors)

    def test_the_dark_night(self):
        string = 'It’s an action movie with Christian Bale act as the bad Joker. It’s a movie in 2008. One of the quotes is "Some men just want to watch the world burn"'
        expected = Query(quotes=["some men just want to watch the world burn"],
                         years=[2007, 2008, 2009],
                         actors=['christian bale'],
                         genres=["action"],
                         tags=['bad', 'joker'])
        actual = parse(string)
        self.assertTrue(actual.quotes == expected.quotes)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.actors == expected.actors)
        self.assertTrue(actual.genres == expected.genres)
        self.assertTrue(actual.tags == expected.tags)

    def test_shawshank(self):
        string = 'It is an old movie, talking about an imprisoned man successfully escaped from prison. The key actor’s name is Tim Robbins with a quote of "I have to remind myself that some birds aren’t meant to be caged."'
        expected = Query(quotes=["i have to remind myself that some birds aren’t meant to be caged."],
                         years=[],
                         actors=['tim robbins'],
                         genres=[],
                         tags=['old', 'imprisoned', 'man', 'successfully', 'escaped', 'prison', ]
                         )
        actual = parse(string)
        self.assertTrue(actual.quotes == expected.quotes)
        self.assertTrue(actual.tags == expected.tags)
        self.assertTrue(actual.actors == expected.actors)

    def test_batman_begins(self):
        string = 'It’s a story about Batman defeat the corruption in Gotham City. The actor’s name is Christian Bale.'
        expected = Query(quotes=[],
                         years=[],
                         genres=[],
                         actors=['christian bale'],
                         tags=['batman', 'defeat', 'corruption', 'gotham', 'city']
                         )
        actual = parse(string)
        self.assertTrue(actual.quotes == expected.quotes)
        self.assertTrue(actual.tags == expected.tags)
        self.assertTrue(actual.actors == expected.actors)

    def test_snowpiercer(self):
        string = "The one where the world has ended and everyone is on a train from 2017"
        expected = Query(quotes=[],
                         years=[2016, 2017, 2018],
                         genres=[],
                         tags=['world', 'ended', 'everyone', 'train']
                         )
        actual = parse(string)
        self.assertTrue(actual.years == expected.years)
        self.assertTrue(actual.tags == expected.tags)

if __name__ == '__main__':
    unittest.main()
