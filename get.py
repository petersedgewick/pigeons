from bs4 import BeautifulSoup
from imdb import IMDb
import pprint
import requests
import shutil
import re
import json
import urllib.request
import os, sys
from PIL import Image, ImageOps

class GetStill():

    def __init__(self):
        pass

    def resizeImage(self, image, path):

        width, height = image.size

        if width > height:
            left = width - height 
            left = left / 2
            right = left
            top = 0
            bottom  = 0
            image1 = image.crop((left, top, right, bottom))
            image2 = image.resize((256,256),Image.ANTIALIAS)
            image2.save(path,optimize=True,quality=85)

        else:
            left = 0
            right = left
            top = height - width
            top = top / 2
            bottom  = top
            image1 = image.crop((left, top, right, bottom))
            image2 = image.resize((256,256),Image.ANTIALIAS)
            image2.save(path,optimize=True,quality=85)



    def download(self, movie):
        pp = pprint.PrettyPrinter(indent=4)

        ia = IMDb()

        BASE_URL = "https://www.imdb.com{}"
        URL = "https://www.imdb.com/title/tt{}/mediaindex?ref_=tt_pv_mi_sm/"

        result = ia.search_movie(movie)

        firstResult = result[0]
        movieId = firstResult.movieID

        URL_MOVIE = URL.format(movieId)

        r = requests.get(URL_MOVIE)

        soup = BeautifulSoup(r.text, "html.parser")
        path = "../" + movie

        os.makedirs(path, exist_ok=True)

        div = soup.find(class_="media_index_thumb_list")
        links = div.findAll('a', href=True)

        for i, link in enumerate(links):
            NEW_URL = BASE_URL.format(link['href'])
            img = link.find('img')
            source = img['src'].split('@')
            try:
                source = source[0] + '@.jpg'
                response = requests.get(source, stream=True)
                response.raise_for_status()
            except:
                source = img['src'].split('@')
                source = source[0] + '@@.jpg'
                response = requests.get(source, stream=True)
            filename = "../" + movie + "/" + movie + '-' + str(i) + '.jpg'
            with open(filename, 'wb') as file:
                shutil.copyfileobj(response.raw, file)   
            try:
                image = Image.open(filename)   
                self.resizeImage(image, filename)
            except:
                pass

