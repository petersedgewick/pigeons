from imdb import IMDb
import requests
import json
from pathlib import Path
from multiprocessing import Pool

PROCESSES = 4

outlist = []

ia = IMDb()

paths = list(Path("../IMSDb/scripts").iterdir())


chunked_paths = [paths[i::PROCESSES] for i in range(PROCESSES)]


def paths_to_dicts(paths):

    out_dicts = []
    count = 0

    for path in paths:
        searchQuerey = path.name.replace(".txt", "").replace("-", " ")

        data = {}
        data['filename'] = str(path.name)

        try:
            searchResult = ia.search_movie(searchQuerey)[0].movieID
            movie = ia.get_movie(searchResult)
        except IndexError as e:
            print(path.name, e)
            out_dicts.append(data)
            continue

        try:
            data['movieID'] = searchResult
        except KeyError as e:
            print(path.name, e)
            out_dicts.append(data)
            continue

        try:
            data['cast'] = [str(person) for person in movie['cast']]
            data['genre'] = movie['genre']
            data['title'] = str(movie['title'])
            data['year'] = movie['year']
        except KeyError as e:
            print(path.name, e)
            pass

        out_dicts.append(data)
        count += 1
        print(count)

    return out_dicts


with Pool(PROCESSES) as p:
    json.dump(p.map(paths_to_dicts, chunked_paths), Path("imdb.json").open('w'))

# ['cast', 'genres', 'runtimes', 'countries', 'country codes', 'language codes', 'color info', 'aspect ratio', 'sound mix', 'box office', 'certificates', 'original air date', 'rating', 'votes', 'cover url', 'plot outline', 'languages', 'title', 'year', 'kind', 'directors', 'writers', 'producers', 'composers', 'cinematographers', 'editors', 'editorial department', 'production designers', 'art directors', 'costume designers', 'make up department', 'production managers', 'assistant directors', 'art department', 'sound department', 'camera department', 'casting department', 'costume departmen', 'music department', 'script department', 'miscellaneous', 'akas', 'writer', 'director', 'production companies', 'distributors', 'other companies', 'plot', 'synopsis', 'canonical title', 'long imdb title', 'long imdb canonical title', 'smart canonical title', 'smart long imdb canonical title', 'full-size cover url']
