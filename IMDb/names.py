import json

actors = []

with open("imdb2.json", 'r') as fp:
    imdbj = json.load(fp)
    for i in imdbj:
        if 'cast' in i:
            actors += [str(x).lower().replace('-','').replace("'", '').replace('.','') for x in i['cast']]
    fp.close()

print(set(actors))