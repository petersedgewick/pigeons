import json

genres = []

with open("imdb2.json", 'r') as fp:
    imdbj = json.load(fp)
    for i in imdbj:
        if 'genre' in i:
            genres += [x.lower().replace('-','').replace("'", '').replace('.','') for x in i['genre']]
    fp.close()



print(set(genres))