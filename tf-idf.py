#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from input import clean_string_split
from pathlib import Path
from string import punctuation, printable

from gensim.corpora import Dictionary
from gensim.models import TfidfModel
# from gensim.similarities import MatrixSimilarity
# from gensim.matutils import corpus2dense, jensen_shannon

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.DEBUG)

from gensim.corpora import Dictionary
from gensim.models import TfidfModel

MODEL_PATH = "."
OUT_PATH = "."
SCRIPT_DIR = "./IMSDb/scripts"

def clean_word(string):
    for p in punctuation:
        string = string.replace(p, '')
    return string.lower()

script_paths = Path(SCRIPT_DIR).iterdir()

# check which files are invalidly encoded - check them.
scripts = [clean_string_split(f.open("r", errors='backslashreplace').read()) for f in script_paths]
print("scripts retrieved")
scripts = [[clean_word(word) for word in script] for script in scripts]
print("scripts cleaned")

# # script_names = [f.stem for f in script_paths]
# # assert len(script_names) != 0

dct = Dictionary(scripts)
dct.save(str(Path(MODEL_PATH) / 'ScriptDict'))
# dct = Dictionary.load(str(DICT_PATH))

corpus = [dct.doc2bow(script) for script in scripts]
print("training model")

model = TfidfModel(corpus)
model.save(str(Path(MODEL_PATH) / 'TfidfModel'))
# # model = TfidfModel.load(str(MODEL_PATH / 'TfidfModel'))
print("done")
# corpus_tfidf = model[corpus]
# print(corpus_tfidf)

# index = MatrixSimilarity(corpus_tfidf)
# sims = index[corpus_tfidf]
