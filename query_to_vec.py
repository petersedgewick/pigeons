from input import Query
from json import load
from enums import *
from create_movies import get_movies


class Vec():

    def __init__(self, query, movie):
        self.movie = movie
        self.actor_score = len(set(query.actors) & set(movie.actors))
        self.year_in_range = movie.year in query.years
        self.genre_score = len(set(query.genres) & set(movie.genres))
        self.quote_score = 0
        # len(x for x in (q for q in query.quotes if q in movie.searchScript(tag))
        self.character_score = len(set(query.tags) & set(movie.characters))
        self.title_score = len(set(query.tags) & set(movie.title.split(' ')))
        # self.tfidf_script = max(movie.script_tf_idf[tag] for query in tags)
        # self.tfidf_imgs = max(movie.img_tf_idf[tag] for query in tags)

    def total_sum(self):
        return sum([self.actor_score, self.year_in_range, self.genre_score, self.quote_score,
                   self.character_score, self.title_score])
    def __str__(self):
        return self.movie.title

def norm(weight, minimum, maximum):
    if (minimum == maximum):
        return 0
    return (weight - minimum) / (maximum - minimum)

def argmax_vec(query):

    vecs = [Vec(query, movie) for movie in get_movies()]

    maxs = {}
    mins = {}

    maxs['actor_score'] = max(v.actor_score for v in vecs)
    maxs['year_in_range'] = max(v.year_in_range for v in vecs)
    maxs['genre_score'] = max(v.genre_score for v in vecs)
    maxs['quote_score'] = max(v.quote_score for v in vecs)
    maxs['character_score'] = max(v.character_score for v in vecs)
    maxs['title_score'] = max(v.title_score for v in vecs)
    # maxs['tfidf_script'] = max(v.tfidf_script for v in vecs)
    # maxs['tfidf_imgs'] = max(v.tfidf_img for v in vecs)

    mins['actor_score'] = min(v.actor_score for v in vecs)
    mins['year_in_range'] = min(v.year_in_range for v in vecs)
    mins['genre_score'] = min(v.genre_score for v in vecs)
    mins['quote_score'] = min(v.quote_score for v in vecs)
    mins['character_score'] = min(v.character_score for v in vecs)
    mins['title_score'] = min(v.title_score for v in vecs)
    # mins['tfidf_script'] = min(v.tfidf_script for v in vecs)
    # mins['tfidf_imgs'] = min(v.tfidf_img for v in vecs)

    for v in vecs:
        v.actor_score = norm(v.actor_score, maxs['actor_score'], mins['actor_score'])
        v.year_in_range = norm(v.year_in_range, maxs['year_in_range'], mins['year_in_range'])
        v.genre_score = norm(v.genre_score, maxs['genre_score'], mins['genre_score'])
        v.quote_score = norm(v.quote_score, maxs['quote_score'], mins['quote_score'])
        v.character_score = norm(v.character_score, maxs['character_score'], mins['character_score'])
        v.title_score = norm(v.title_score, maxs['title_score'], mins['title_score'])
        # v.tfidf_script = norm(v.tfidf_script, maxs['tfidf_script'], mins['tfidf_script'])
        # v.tfidf_imgs = norm(v.tfidf_imgs, maxs['tfidf_imgs'], mins['tfidf_imgs'])

    return max(vecs, key=lambda arg: arg.total_sum())


print(argmax_vec(Query(quotes=["is it just me or is it getting crazier out there?"],
                 years=[1960, 1961, 1962, 1963, 1964, 1965, 1966, 1967, 1968, 1969],
                 genres=["thriller", "action"],
                 tags=["joker"])))


