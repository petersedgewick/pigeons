import requests
from bs4 import BeautifulSoup


class Content():

    def __init__(self):
        self.StreamHeaders = {
                            'x-rapidapi-host': "utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com",
                            'x-rapidapi-key': "lIJo5Q1kzGmshf8lwxIvU1HDNSzqp1tfN19jsnLfUpEhQKexwO"
                            }

        self.StreamUrl =  "https://utelly-tv-shows-and-movies-availability-v1.p.rapidapi.com/lookup"

        self.imbdUrl = "https://www.imdb.com/title/tt{}/"

        self.imdbBaseUrl = "https://www.imdb.com{}"


    def getStream(self, name):

        querystring = {"term": name,"country":"uk"}

        response = requests.get(self.StreamUrl, headers=self.StreamHeaders, params=querystring)

        jsonText = response.json()

        prime = None
        netflix = None

        for result in jsonText['results']:
            for location in result['locations']:
                if location['display_name'] == 'Amazon Prime':
                    prime = location['url']
                elif location['display_name'] == 'Netflix':
                    netflix = location['url']
            break
        
        urls = {
            'prime': prime,
            'netflix': netflix
        }

        return (urls)

    def getTrailer(self, movieID):

        URL = self.imbdUrl.format(movieID)

        request = requests.get(URL)
        soup = BeautifulSoup(request.text, "html.parser")
        a = soup.findAll('a', {'class':'video-modal'})
        href = a[0]['href']
        href = self.imdbBaseUrl.format(href)

        return href        

    def getImage(self, movieID):

        URL = self.imbdUrl.format(movieID)

        request = requests.get(URL)
        soup = BeautifulSoup(request.text, "html.parser")
        a = soup.findAll('div', {'class':'poster'})
        img = a[0]
        links = img.findAll('img')
        href = links[0]['src']

        return href    



# obj = Content()
# obj.getStream("Matrix")
# obj.getTrailer("0133093")
# obj.getImage("0133093")


#       {'prime': 'http://www.amazon.co.uk/gp/product/B00IA54RC4?tag=utellycom00-21', 'netflix': None}

#       https://www.imdb.com/video/imdb/vi1032782617?playlistId=tt0133093

#       https://m.media-amazon.com/images/M/MV5BNzQzOTk3OTAtNDQ0Zi00ZTVkLWI0MTEtMDllZjNkYzNjNTc4L2ltYWdlXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_UX182_CR0,0,182,268_AL_.jpg