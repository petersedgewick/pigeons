import subprocess
import re

DIR = "./IMSDb/scripts/scripts-lower/lowercase"


def grep_scripts(quote: str):
    print("🔎 looking for '" + quote + "' in " + DIR)
    cmd = 'grep -rn "'+quote+'" . | awk {\'print $1\'}'
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)

    clean = []
    for row in process.communicate()[0][:-1].decode("utf-8").split("\n"):
        clean += re.findall(".*/([a-zA-Z-,]*.txt)", row)

    print("🔍 found '" + quote + "' in " + str(clean))
    return clean

if __name__ == '__main__':
    grep_scripts("Is Mason down there?")