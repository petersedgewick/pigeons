from json import load, dump
from pathlib import Path
from input import clean_string
from get_tf_idf import img_tf_idf, tf_idf

IMDB_JSON = "IMDb/imdb2.json"
IMSDB_JSON = "IMSDb/script-data.json"
SCRIPT_DIR = "./IMSDb/scripts"


class Movie():

    def __init__(self, movieId, year=None, actors=None, characters=None, genres=None, title=None, script=None):
        self.year = year
        self.actors = actors
        self.characters = characters
        self.genres = genres
        self.title = title
        # self.script_tf_idf = tf_idf(script)
        # self.img_tf_idf = img_tf_idf(movieId)


    def __str__(self):
        return f'Title: {self.title}, Year: {self.year}, Actors: {self.actors}, Genres: {self.genres}'

def get_movies():

    movies = []

    with Path(IMDB_JSON).open("r") as imdb_f:
        IMDB_data = load(imdb_f)

    flat_IMSDB_data = {}
    with Path(IMSDB_JSON).open("r") as imsdb_f:
        IMSDB_data = load(imsdb_f)

    for x in IMSDB_data:
        flat_IMSDB_data[x["title"]] = list(x["characters"].keys())

    IMSDB= flat_IMSDB_data

    for x in IMDB_data:
        try:
            movie = Movie(x['movieID'],
                          x['year'],
                          {clean_string(actor) for actor in x['cast']},
                          {clean_string(character) for character in IMSDB[x["filename"].replace(".txt", "")]},
                          {clean_string(genre) for genre in x['genre']},
                          x['title'],
                          x['filename']
                         )
            movies.append(movie)
            # print(movie)

        except KeyError as e:
            print(x['filename'], e)

    return movies

