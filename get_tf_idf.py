from pathlib import Path
from gensim.corpora import Dictionary
from gensim.models import TfidfModel
from input import clean_string_split
from json import load

MODEL_PATH = "."

DICT_PATH = "./ScriptDict"
SCRIPT_DIR = "./IMSDb/scripts"

model = TfidfModel.load(str(Path(MODEL_PATH) / 'TfidfModel'))
dic = Dictionary.load(DICT_PATH)



IMG_DICT_PATH = "./ImgDict"
IMG_JSON_PATH = "./tagList.json"

imgmodel = TfidfModel.load(str(Path(MODEL_PATH) / 'ImgModel'))
imgdic = dic = Dictionary.load(IMG_DICT_PATH)
img_data = load(Path(IMG_JSON_PATH).open())


def norm(weight, maximum, minimum):
    return (weight - minimum) / (maximum - minimum)


def tf_idf(script_path):

    doc = clean_string_split((Path(SCRIPT_DIR) / script_path).open().read())
    # print(doc)
    print(dic.doc2bow(doc))
    vector = model[dic.doc2bow(doc)]

    # print()
    # id, token = list(dic.token2id.items())[0]
    # print(vector[id])
    print(dic.doc2bow(doc))

    weights = [vector[id][1] for id, token in dic.doc2bow(doc)]
    minimum = min(weights)
    maximum = max(weights)

    new_vec = {token: norm(vector[id][1], maximum, minimum) for id, token in dic.doc2bow(doc)}

    return new_vec


def img_tf_idf(movieid):

    tagList = next(x["tags"] for x in img_data if x["movieID"] == movieid)

    if not tagList:
        return None

    vector = imgmodel[imgdic.doc2bow(tagList)]

    print("hi!", dic.doc2bow(tagList))

    weights = [vector[id][1] for id, token in dic.doc2bow(tagList)]
    minimum = min(weights)
    maximum = max(weights)

    new_vec = {token: norm(vector[id][1], maximum, minimum) for id, token in dic.doc2bow(tagList)}

    return new_vec
