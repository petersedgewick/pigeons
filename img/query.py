from input import Query


user_input = 'It’s an action movie with Heath Ledger as the evil Joker. It’s a movie in 2008. ' \
         'One of the quotes is "Some men just want to watch the world burn"'

output = Query(
    quotes=["some men just want to watch the world burn"],
    years=[2007, 2008, 2009],
    actors=['heath ledger'],
    genres=["action"],
    tags=['evil', 'joker']
)
